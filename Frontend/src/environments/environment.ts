// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBADY6amuMv6nQEcWqnhpQHvvcmln4gmr8',
    authDomain: 'proep-frontend.firebaseapp.com',
    projectId: 'proep-frontend',
    storageBucket: 'proep-frontend.appspot.com',
    messagingSenderId: '524508019826',
    appId: '1:524508019826:web:3b2f20b48ed48d301f16eb'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/dist/zone-error';  // Included with Angular CLI.
