import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-jobs',
  templateUrl: './list-jobs.component.html',
  styleUrls: ['./list-jobs.component.css']
})
export class ListJobsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }


  isShowAllJobs = false;
  isToggledJobs = true;

  toggleAllJobs() {
    this.isShowAllJobs = true;
    this.isToggledJobs = false;
  }
}
