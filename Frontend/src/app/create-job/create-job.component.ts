import { Component, OnInit } from '@angular/core';
import { Job } from 'src/models/job';
import { User } from 'src/models/user';
import { GetApiService } from '../get-api.service';

@Component({
  selector: 'app-create-job',
  templateUrl: './create-job.component.html',
  styleUrls: ['./create-job.component.css']
})
export class CreateJobComponent implements OnInit {

  newJob!: Job;
  errorMessage!: string;
  constructor(private api: GetApiService) { }

  isShowChooseCategories = true;
  isShowDetails = false;
  isShowPostJob = false;
  ngOnInit(): void {

    this.newJob = new Job();
    let dateTime = new Date();
    this.newJob.dateCreated = dateTime;
    
  }


  toggleDetails() {
    this.isShowChooseCategories = false;
    this.isShowDetails = true;
    this.newJob.providerId = this.api.user.id;
  }

  togglePostJob() {
    this.isShowDetails = false;
    this.isShowPostJob = true;
    console.log(this.newJob);
    this.addJob(this.newJob);

  }

  addJob(job:Job){
    
    this.api.postJob(job, this.api.user.idToken).subscribe({
      next: data => {
      },
      error: error => {
          this.errorMessage = error.message;
          console.error('There was an error!', error);
      }
  })
  }
  


}
