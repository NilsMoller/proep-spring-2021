import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { HomePageComponent } from './home-page/home-page.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { ListJobsComponent } from './list-jobs/list-jobs.component';
import { CreateJobComponent } from './create-job/create-job.component';
import { JobComponent } from './job/job.component';
import { JobsPageComponent } from './jobs-page/jobs-page.component';
import { TermsComponent } from './terms/terms.component';


const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'jobs', component: JobsPageComponent},
  {path: 'jobsMihai', component: ListJobsComponent},
  {path:'jobs/:id', component: JobComponent},
  {path: 'profile', component: ProfilePageComponent},
  {path: 'create-job', component: CreateJobComponent},
  {path: 'profile/:id', component: ProfilePageComponent},
  {path: 'privacy', component: TermsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

