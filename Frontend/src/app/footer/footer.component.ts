import { Component, OnInit } from '@angular/core';
import {faInfoCircle, faWrench, faUserAlt} from '@fortawesome/free-solid-svg-icons';
import { GetApiService } from '../get-api.service';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  faInfoCircle = faInfoCircle;
  faWrench = faWrench;
  faUserAlt = faUserAlt;

  constructor(public api: GetApiService) { }

  ngOnInit(): void {
  }

}
