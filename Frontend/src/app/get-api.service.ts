import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Job } from 'src/models/job';
import { Review } from 'src/models/review';
import { User } from 'src/models/user';
import { HeaderComponent } from './header/header.component';
import { SocialUser } from 'angularx-social-login';
import { AnonymousSubject } from 'rxjs/internal/Subject';
import { Booking } from 'src/models/booking';
import { stringify } from '@angular/compiler/src/util';
@Injectable({
  providedIn: 'root'
})
export class GetApiService {

  public user!: SocialUser;
  private httpOptions = {
    responseType: 'text'
  };

  constructor(
    private http:HttpClient
  ) { }

  pingCall()
  {
    return this.http.get('http://proep.api.nilsmoller.com/Ping', {responseType: 'text'});
  }
  getJobs()
  {
    return this.http.get<any[]>('http://proep.api.nilsmoller.com/Jobs');
  }
  getJob(id: string){
    return this.http.get<any>('http://proep.api.nilsmoller.com/Jobs/' + id);
  }

  postJob(job:Job, id: string){
    const headers = 
    { 
      'Authorization': id,
      'content-type': 'application/json'
    }  
    // 'authorization' : 'tokenid from google'.
    const body=JSON.stringify(job);
    return this.http.post('http://proep.api.nilsmoller.com/Jobs', body,{'headers':headers , observe: 'response'})
  }

  getJobFromUser(id: string){
    return this.http.get<any>('http://proep.api.nilsmoller.com/Jobs/user/' + id);
  }
  getUser(id: string){
    return this.http.get<any>('http://proep.api.nilsmoller.com/Users/' + id);
  }

  
  // R E V I E W S

  // Get all reviews
  // postReview(review: Review){

  //   return this.http.post<any>('http://proep.api.nilsmoller.com/Reviews/reviewerId/');
  // }
  
  // Get all reviews that specific user created.
  getReviewAsReviewer(id: string){
    return this.http.get<any>('http://proep.api.nilsmoller.com/Reviews/reviewerId/' + id);
  }

  // Get all reviews that specific user received.
  getReviewAsReviewee(id: string){
    return this.http.get<any>('http://proep.api.nilsmoller.com/Reviews/revieweeId/' + id);
  }
  getReviewsForRevieweeAsReviewer(revieweeId: string, reviewerId:string ){
    console.log("Reviewer: ", reviewerId);
    console.log("Reviewee: ", revieweeId);
    return this.http.get<any>('http://proep.api.nilsmoller.com/Reviews/' + reviewerId + '/' + revieweeId);
  }
  postReview(review: Review){
    const headers = {'Authorization': this.user.idToken,'content-type': 'application/json'}  
   const body= JSON.stringify(review);
    return this.http.post('http://proep.api.nilsmoller.com/Reviews', body,{'headers':headers , observe: 'response'});
  }



  // Bookings

  //Get Booking By Booking ID
  getBookingById(id: string){
    return this.http.get<any>('http://proep.api.nilsmoller.com/Bookings/' + id);
  }

  //Get Bookings By JobId
  getBookingsByJobID(jobid: string){
    return this.http.get<any>('http://proep.api.nilsmoller.com/Bookings/job/' + jobid);
  }
  
  postBooking(booking: Booking){
    const headers = {'Authorization': this.user.idToken,'content-type': 'application/json'}  

   const body= JSON.stringify(booking);
    return this.http.post('http://proep.api.nilsmoller.com/Bookings', body,{'headers':headers , observe: 'response'});
  }


  

  deleteJob(id: string){

    const headers = {'Authorization': this.user.idToken, 'content-type': 'application/json'}

    return this.http.delete('http://proep.api.nilsmoller.com/Jobs/' + id, {'headers': headers});
  
  }

  postUserRegister(user: User, token: string){
    const headers = {'Authorization': token,'content-type': 'application/json'}  
    // 'authorization' : 'tokenid from google'.
  
   const body= JSON.stringify(user);
    console.log("Non stringified body:");
    console.log(user);
    console.log("Stringified body:");
    console.log(body);
    console.log(headers);
    return this.http.post('http://proep.api.nilsmoller.com/Users/auth/google/register', body,{'headers':headers , observe: 'response'})
  }


 

  
 
  
}
