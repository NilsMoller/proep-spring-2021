﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.DataTypes;
using Core.Models;

using Microsoft.EntityFrameworkCore;

using Xunit;

namespace API.Db.Tests
{
    public class JobDbHandlerTests : IDisposable
    {
        private DbContextOptions<DbContext> Options { get; }
        
        // setup
        public JobDbHandlerTests()
        {
            this.Options = new DbContextOptionsBuilder<DbContext>()
            .UseInMemoryDatabase(databaseName: "ProEPDatabase")
            .Options;

            using (DbContext context = new DbContext(this.Options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                context.Jobs.Add(new Job { JobId = Guid.Empty });
                context.SaveChanges();
            }
        }

        // breakdown
        public void Dispose()
        {

        }


        [Fact]
        public async Task GetJobsAsync_ReturnsList()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                var userData = new[]
                {
                new
                {
                    UserId = "110811874816751076279",
                    Email = "flambshine0@imgur.com",
                    FirstName = "Frazer",
                    LastName = "Lambshine",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2000, 5, 2),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(976) 5971010",
                    Address = "9 Golden Leaf Trail",
                    City = "Balayong",
                    Rating = 2f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "110811874816751097523",
                    Email = "sgillivrie1@utexas.edu",
                    FirstName = "Sophie",
                    LastName = "Gillivrie",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1995, 12, 5),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(894) 7687284",
                    Address = "23559 Dexter Center",
                    City = "Barobo",
                    Rating = 1f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "954853874816751076279",
                    Email = "vebbett2@odnoklassniki.ru",
                    FirstName = "Veronike",
                    LastName = "Ebbett",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1950, 2, 26),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/ff4444/ffffff",
                    Phone = "(180) 4593004",
                    Address = "056 Springview Terrace",
                    City = "Shchigry",
                    Rating = 3f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "956325694785213654589",
                    Email = "wengel3@diigo.com",
                    FirstName = "Weidar",
                    LastName = "Engel",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1970, 8, 15),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/ff4444/ffffff",
                    Phone = "(282) 5803398",
                    Address = "0 Pierstorff Trail",
                    City = "Kiambu",
                    Rating = 0.2f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "9563256947852136598520",
                    Email = "sdevaney4@histats.com",
                    FirstName = "Shayna",
                    LastName = "Devaney",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2004, 7, 18),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/cc0000/ffffff",
                    Phone = "(971) 6571942",
                    Address = "0 Anhalt Drive",
                    City = "Kudanding",
                    Rating = 4.8f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "850132544785213654589",
                    Email = "nstrewther5@nsw.gov.au",
                    FirstName = "Norean",
                    LastName = "Strewther",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2001, 3, 9),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(499) 6865801",
                    Address = "30 Cottonwood Drive",
                    City = "Teki Szlacheckie",
                    Rating = 4f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "850132958321456545893",
                    Email = "gdurn6@youku.com",
                    FirstName = "Gertrud",
                    LastName = "Durn",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1999, 9, 9),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(973) 4431447",
                    Address = "1114 Village Green Parkway",
                    City = "Muzhou",
                    Rating = 3.9f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "118269202694979787959",
                    Email = "nils.moller2000@gmail.com",
                    FirstName = "Nils",
                    LastName = "Möller",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2000, 5, 2),
                    ProfilePictureLink = "https://lh3.googleusercontent.com/a/AATXAJyaUEquS0bkZVf6rQ7bM4l8dcZ0S9_GUiRYENZKGQ=s96-c",
                    Phone = "+31642868029",
                    Address = "Heezerweg 117, 5614HC",
                    City = "Eindhoven",
                    Rating = 5f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImQzZmZiYjhhZGUwMWJiNGZhMmYyNWNmYjEwOGNjZWI4ODM0MDZkYWMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI0MDc0MDg3MTgxOTIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI0MDc0MDg3MTgxOTIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTgyNjkyMDI2OTQ5Nzk3ODc5NTkiLCJlbWFpbCI6Im5pbHMubW9sbGVyMjAwMEBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6IkhXLUx0eW14enVJNnpYYVVvMkIyQXciLCJuYW1lIjoiTmlscyIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vYS9BQVRYQUp5YVVFcXVTMGJrWlZmNnJRN2JNNGw4ZGNaMFM5X0dVaVJZRU5aS0dRPXM5Ni1jIiwiZ2l2ZW5fbmFtZSI6Ik5pbHMiLCJsb2NhbGUiOiJlbiIsImlhdCI6MTYyMTI1ODcwMCwiZXhwIjoxNjIxMjYyMzAwfQ.DjanpzA9Ws5AZC5PRnszuWcpXgyyHyPYh2B43jTmxCMEws_9ZiIzKn3xsMjuYcPfO6GwSWRdzYUHBTP0sUALCtlqtMMlRJhr6MF7cZ8Pr-hDvnEoc-mwroCdU6D_Td_CV4zzgvidCnzvvtuiY6hKzYb5mMcAtFyuiusES0HYjI3uapmeLRLmpNljWN4DFFQfLnq-RB6SkfKQYNhhXdU2xFsVyRccO5AZYYX3JWtKvCDRckyIsnxckjGnYkYlDAwnywex5WWKR3Jx6zR2dv6hb5kkx3DXy-e_CFApDXPdBtx_uB_39J5m3jvrFxq16Y1mrswuaw15k3Yt-VUCN7gjlQ"
                }
            };
                Job job1 = new Job
                {
                    JobId = Guid.Parse("debe96be-f90d-4677-8203-aba156b5ef74"),
                    ProviderId = userData[0].UserId,
                    Price = 5.32,
                    Title = "Business analyst",
                    Description = "Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.\n\nNullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.",
                    LevelOfExperience = LEVEL_OF_EXPERIENCE.Expert,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now
                };

                Job job2 = new Job
                {
                    JobId = Guid.Parse("dba2cb60-c1db-4317-86e0-fe1b17c73b0a"),
                    ProviderId = userData[0].UserId,
                    Price = 27.12,
                    Title = "Photographer",
                    Description = "Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.\n\nPhasellus in felis. Donec semper sapien a libero. Nam dui.\n\nProin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.",
                    LevelOfExperience = LEVEL_OF_EXPERIENCE.Intermediate,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now

                };

                Job job3 = new Job
                {
                    JobId = Guid.Parse("410e6995-9b05-46e7-972b-15f34dc47b3e"),
                    ProviderId = userData[1].UserId,
                    Price = 70.00,
                    Title = "Plumming",
                    Description = "Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.\n\nSed ante. Vivamus tortor. Duis mattis egestas metus.",
                    LevelOfExperience = LEVEL_OF_EXPERIENCE.Beginner,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now
                };

                JobDbHandler jobDbHandler = new JobDbHandler(context);
                List<Job> jobList = new List<Job>();
                jobList.Add(job1);
                jobList.Add(job2);
                jobList.Add(job3);
                jobList.Add(new Job { JobId = Guid.Empty });
                // Act
                var myresult = await jobDbHandler.GetJobsAsync();

                // Assert 

                Assert.Equal(jobList, myresult);
            }
            
        }

        [Fact]
        public async Task GetJobs_ReturnsList()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                var userData = new[]
                {
                new
                {
                    UserId = "110811874816751076279",
                    Email = "flambshine0@imgur.com",
                    FirstName = "Frazer",
                    LastName = "Lambshine",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2000, 5, 2),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(976) 5971010",
                    Address = "9 Golden Leaf Trail",
                    City = "Balayong",
                    Rating = 2f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "110811874816751097523",
                    Email = "sgillivrie1@utexas.edu",
                    FirstName = "Sophie",
                    LastName = "Gillivrie",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1995, 12, 5),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(894) 7687284",
                    Address = "23559 Dexter Center",
                    City = "Barobo",
                    Rating = 1f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "954853874816751076279",
                    Email = "vebbett2@odnoklassniki.ru",
                    FirstName = "Veronike",
                    LastName = "Ebbett",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1950, 2, 26),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/ff4444/ffffff",
                    Phone = "(180) 4593004",
                    Address = "056 Springview Terrace",
                    City = "Shchigry",
                    Rating = 3f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "956325694785213654589",
                    Email = "wengel3@diigo.com",
                    FirstName = "Weidar",
                    LastName = "Engel",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1970, 8, 15),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/ff4444/ffffff",
                    Phone = "(282) 5803398",
                    Address = "0 Pierstorff Trail",
                    City = "Kiambu",
                    Rating = 0.2f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "9563256947852136598520",
                    Email = "sdevaney4@histats.com",
                    FirstName = "Shayna",
                    LastName = "Devaney",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2004, 7, 18),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/cc0000/ffffff",
                    Phone = "(971) 6571942",
                    Address = "0 Anhalt Drive",
                    City = "Kudanding",
                    Rating = 4.8f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "850132544785213654589",
                    Email = "nstrewther5@nsw.gov.au",
                    FirstName = "Norean",
                    LastName = "Strewther",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2001, 3, 9),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(499) 6865801",
                    Address = "30 Cottonwood Drive",
                    City = "Teki Szlacheckie",
                    Rating = 4f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "850132958321456545893",
                    Email = "gdurn6@youku.com",
                    FirstName = "Gertrud",
                    LastName = "Durn",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1999, 9, 9),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(973) 4431447",
                    Address = "1114 Village Green Parkway",
                    City = "Muzhou",
                    Rating = 3.9f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "118269202694979787959",
                    Email = "nils.moller2000@gmail.com",
                    FirstName = "Nils",
                    LastName = "Möller",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2000, 5, 2),
                    ProfilePictureLink = "https://lh3.googleusercontent.com/a/AATXAJyaUEquS0bkZVf6rQ7bM4l8dcZ0S9_GUiRYENZKGQ=s96-c",
                    Phone = "+31642868029",
                    Address = "Heezerweg 117, 5614HC",
                    City = "Eindhoven",
                    Rating = 5f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImQzZmZiYjhhZGUwMWJiNGZhMmYyNWNmYjEwOGNjZWI4ODM0MDZkYWMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI0MDc0MDg3MTgxOTIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI0MDc0MDg3MTgxOTIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTgyNjkyMDI2OTQ5Nzk3ODc5NTkiLCJlbWFpbCI6Im5pbHMubW9sbGVyMjAwMEBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6IkhXLUx0eW14enVJNnpYYVVvMkIyQXciLCJuYW1lIjoiTmlscyIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vYS9BQVRYQUp5YVVFcXVTMGJrWlZmNnJRN2JNNGw4ZGNaMFM5X0dVaVJZRU5aS0dRPXM5Ni1jIiwiZ2l2ZW5fbmFtZSI6Ik5pbHMiLCJsb2NhbGUiOiJlbiIsImlhdCI6MTYyMTI1ODcwMCwiZXhwIjoxNjIxMjYyMzAwfQ.DjanpzA9Ws5AZC5PRnszuWcpXgyyHyPYh2B43jTmxCMEws_9ZiIzKn3xsMjuYcPfO6GwSWRdzYUHBTP0sUALCtlqtMMlRJhr6MF7cZ8Pr-hDvnEoc-mwroCdU6D_Td_CV4zzgvidCnzvvtuiY6hKzYb5mMcAtFyuiusES0HYjI3uapmeLRLmpNljWN4DFFQfLnq-RB6SkfKQYNhhXdU2xFsVyRccO5AZYYX3JWtKvCDRckyIsnxckjGnYkYlDAwnywex5WWKR3Jx6zR2dv6hb5kkx3DXy-e_CFApDXPdBtx_uB_39J5m3jvrFxq16Y1mrswuaw15k3Yt-VUCN7gjlQ"
                }
            };
                Job job1 = new Job
                {
                    JobId = Guid.Parse("debe96be-f90d-4677-8203-aba156b5ef74"),
                    ProviderId = userData[0].UserId,
                    Price = 5.32,
                    Title = "Business analyst",
                    Description = "Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.\n\nNullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.",
                    LevelOfExperience = LEVEL_OF_EXPERIENCE.Expert,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now
                };

                Job job2 = new Job
                {
                    JobId = Guid.Parse("dba2cb60-c1db-4317-86e0-fe1b17c73b0a"),
                    ProviderId = userData[0].UserId,
                    Price = 27.12,
                    Title = "Photographer",
                    Description = "Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.\n\nPhasellus in felis. Donec semper sapien a libero. Nam dui.\n\nProin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.",
                    LevelOfExperience = LEVEL_OF_EXPERIENCE.Intermediate,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now

                };

                Job job3 = new Job
                {
                    JobId = Guid.Parse("410e6995-9b05-46e7-972b-15f34dc47b3e"),
                    ProviderId = userData[1].UserId,
                    Price = 70.00,
                    Title = "Plumming",
                    Description = "Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.\n\nSed ante. Vivamus tortor. Duis mattis egestas metus.",
                    LevelOfExperience = LEVEL_OF_EXPERIENCE.Beginner,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now
                };

                JobDbHandler jobDbHandler = new JobDbHandler(context);
                List<Job> jobList = new List<Job>();
                jobList.Add(job1);
                jobList.Add(job2);
                jobList.Add(job3);
                jobList.Add(new Job { JobId = Guid.Empty });

                // Act
                var myresult = jobDbHandler.GetJobs();

                // Assert 

                Assert.Equal(jobList, myresult);
            }
            
        }

        [Fact]
        public async Task GetJobAsync_WhenIdExists_ReturnsJob()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                JobDbHandler jobDbHandler = new JobDbHandler(context);

                // Act
                var myresult = await jobDbHandler.GetJobAsync(Guid.Empty);
                // Assert
                Assert.Equal(new Job { JobId = Guid.Empty }, myresult);
            }
            
        }

        [Fact] 
        public async Task GetJobAsync_WhenIdDoesNotExist_ThrowsArgumentException()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                JobDbHandler jobDbHandler = new JobDbHandler(context);

                // Act & Assert
                await Assert.ThrowsAsync<ArgumentException>(async () => await jobDbHandler.GetJobAsync(Guid.NewGuid()));
            }

        }

        [Fact]
        public async Task GetJob_WhenIdExists_ReturnsJob()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                JobDbHandler jobDbHandler = new JobDbHandler(context);

                // Act
                var myresult = jobDbHandler.GetJob(Guid.Empty);

                // Assert
                Assert.Equal(new Job { JobId = Guid.Empty }, myresult);
            }
            
        }

        [Fact]
        public async Task GetJob_WhenIdDoesNotExist_ThrowsArgumentException()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                JobDbHandler jobDbHandler = new JobDbHandler(context);
                // Act & Assert
                Assert.Throws<ArgumentException>(() => jobDbHandler.GetJob(Guid.NewGuid()));
            }
            
        }

        [Fact]
        public async Task CreateJobAsync_AssertTrue()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                JobDbHandler jobDbHandler = new JobDbHandler(context);
                Guid jobID = Guid.NewGuid();
                // Act
                await jobDbHandler.CreateJobAsync(new Job { JobId = jobID });

                //Assert
                Assert.True(await context.Jobs.ContainsAsync(new Job { JobId = jobID }));
            }
           
        }

        [Fact]
        public async Task CreateJob_AssertTrue()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                JobDbHandler jobDbHandler = new JobDbHandler(context);
                Guid jobID = Guid.NewGuid();
                // Act
                jobDbHandler.CreateJob(new Job { JobId = jobID });

                //Assert
                Assert.True(context.Jobs.Contains(new Job { JobId = jobID }));
            }
            
        }

        [Fact]
        public async Task UpdateJobAsync_WhenIdExists_UpdatesJob()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                Job _updatedJob = new Job { JobId = Guid.Empty, Title = "Lorem" };
                JobDbHandler jobDbHandler = new JobDbHandler(context);

                // Act
                await jobDbHandler.UpdateJobAsync(_updatedJob);

                // Assert
                Assert.Equal(await context.Jobs.FirstOrDefaultAsync(job => job.JobId == Guid.Empty), _updatedJob);
            }
            
        }

        [Fact]
        public async Task UpdateJobAsync_WhenIdDoesNotExist_ThrowsArgumentException()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                JobDbHandler jobDbHandler = new JobDbHandler(context);

                // Assert
                await Assert.ThrowsAsync<ArgumentException>(async () => await jobDbHandler.UpdateJobAsync(new Job { JobId = Guid.NewGuid() }));

            }
        }

        [Fact]
        public async Task DeleteJobAsync_ReturnsOk()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                JobDbHandler jobDbHandler = new JobDbHandler(context);

                // Act
                await jobDbHandler.DeleteJobAsync(Guid.Empty);

                // Assert
                Assert.False(await context.Jobs.ContainsAsync(new Job { JobId = Guid.Empty }));
            }
        }

        [Fact]
        public async Task DeleteJobAsync_WhenIdDoesNotExist_ThrowsArgumentException()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                JobDbHandler jobDbHandler = new JobDbHandler(context);

                // Act & Assert
                await Assert.ThrowsAsync<ArgumentException>(async () => await jobDbHandler.DeleteJobAsync(Guid.NewGuid()));
            }
        }

        [Fact]
        public async Task DeleteJob_ReturnsOk()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                JobDbHandler jobDbHandler = new JobDbHandler(context);

                // Act
                jobDbHandler.DeleteJob(Guid.Empty);

                // Assert
                Assert.False(context.Jobs.Any(job => job.JobId == Guid.Empty));
            }
        }

        [Fact]
        public async Task DeleteJob_WhenIdDoesNotExist_ThrowsArgumentException()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                JobDbHandler jobDbHandler = new JobDbHandler(context);

                // Act & Assert
                Assert.Throws<ArgumentException>(() => jobDbHandler.DeleteJob(Guid.NewGuid()));
            }
        }
    }
}