﻿
using Core.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace API.Db.Tests
{
    public class ReviewDbHandlerTests : IDisposable
    {
        private DbContextOptions<DbContext> Options { get; }

        // setup
        public ReviewDbHandlerTests()
        {
            this.Options = new DbContextOptionsBuilder<DbContext>()
            .UseInMemoryDatabase(databaseName: "ProEPDatabase")
            .Options;

            using (DbContext context = new DbContext(this.Options))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                context.Reviews.Add(new Review {ReviewId = Guid.Empty });
                context.SaveChanges();
            }
        }

        // breakdown
        public void Dispose()
        {

        }

        [Fact]
        public async Task GetReviewsAsync_ReturnsList()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                var userData = new[]
            {
                new
                {
                    UserId = "110811874816751076279",
                    Email = "flambshine0@imgur.com",
                    FirstName = "Frazer",
                    LastName = "Lambshine",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2000, 5, 2),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(976) 5971010",
                    Address = "9 Golden Leaf Trail",
                    City = "Balayong",
                    Rating = 2f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "110811874816751097523",
                    Email = "sgillivrie1@utexas.edu",
                    FirstName = "Sophie",
                    LastName = "Gillivrie",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1995, 12, 5),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(894) 7687284",
                    Address = "23559 Dexter Center",
                    City = "Barobo",
                    Rating = 1f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "954853874816751076279",
                    Email = "vebbett2@odnoklassniki.ru",
                    FirstName = "Veronike",
                    LastName = "Ebbett",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1950, 2, 26),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/ff4444/ffffff",
                    Phone = "(180) 4593004",
                    Address = "056 Springview Terrace",
                    City = "Shchigry",
                    Rating = 3f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "956325694785213654589",
                    Email = "wengel3@diigo.com",
                    FirstName = "Weidar",
                    LastName = "Engel",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1970, 8, 15),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/ff4444/ffffff",
                    Phone = "(282) 5803398",
                    Address = "0 Pierstorff Trail",
                    City = "Kiambu",
                    Rating = 0.2f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "9563256947852136598520",
                    Email = "sdevaney4@histats.com",
                    FirstName = "Shayna",
                    LastName = "Devaney",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2004, 7, 18),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/cc0000/ffffff",
                    Phone = "(971) 6571942",
                    Address = "0 Anhalt Drive",
                    City = "Kudanding",
                    Rating = 4.8f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "850132544785213654589",
                    Email = "nstrewther5@nsw.gov.au",
                    FirstName = "Norean",
                    LastName = "Strewther",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2001, 3, 9),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(499) 6865801",
                    Address = "30 Cottonwood Drive",
                    City = "Teki Szlacheckie",
                    Rating = 4f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "850132958321456545893",
                    Email = "gdurn6@youku.com",
                    FirstName = "Gertrud",
                    LastName = "Durn",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1999, 9, 9),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(973) 4431447",
                    Address = "1114 Village Green Parkway",
                    City = "Muzhou",
                    Rating = 3.9f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "118269202694979787959",
                    Email = "nils.moller2000@gmail.com",
                    FirstName = "Nils",
                    LastName = "Möller",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2000, 5, 2),
                    ProfilePictureLink = "https://lh3.googleusercontent.com/a/AATXAJyaUEquS0bkZVf6rQ7bM4l8dcZ0S9_GUiRYENZKGQ=s96-c",
                    Phone = "+31642868029",
                    Address = "Heezerweg 117, 5614HC",
                    City = "Eindhoven",
                    Rating = 5f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImQzZmZiYjhhZGUwMWJiNGZhMmYyNWNmYjEwOGNjZWI4ODM0MDZkYWMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI0MDc0MDg3MTgxOTIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI0MDc0MDg3MTgxOTIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTgyNjkyMDI2OTQ5Nzk3ODc5NTkiLCJlbWFpbCI6Im5pbHMubW9sbGVyMjAwMEBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6IkhXLUx0eW14enVJNnpYYVVvMkIyQXciLCJuYW1lIjoiTmlscyIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vYS9BQVRYQUp5YVVFcXVTMGJrWlZmNnJRN2JNNGw4ZGNaMFM5X0dVaVJZRU5aS0dRPXM5Ni1jIiwiZ2l2ZW5fbmFtZSI6Ik5pbHMiLCJsb2NhbGUiOiJlbiIsImlhdCI6MTYyMTI1ODcwMCwiZXhwIjoxNjIxMjYyMzAwfQ.DjanpzA9Ws5AZC5PRnszuWcpXgyyHyPYh2B43jTmxCMEws_9ZiIzKn3xsMjuYcPfO6GwSWRdzYUHBTP0sUALCtlqtMMlRJhr6MF7cZ8Pr-hDvnEoc-mwroCdU6D_Td_CV4zzgvidCnzvvtuiY6hKzYb5mMcAtFyuiusES0HYjI3uapmeLRLmpNljWN4DFFQfLnq-RB6SkfKQYNhhXdU2xFsVyRccO5AZYYX3JWtKvCDRckyIsnxckjGnYkYlDAwnywex5WWKR3Jx6zR2dv6hb5kkx3DXy-e_CFApDXPdBtx_uB_39J5m3jvrFxq16Y1mrswuaw15k3Yt-VUCN7gjlQ"
                }
            };

                Review review1 = new Review
                {
                    ReviewId = Guid.Parse("649f29d1-f947-410e-a877-625f2c7cf3e1"),
                    ReviewerId = userData[6].UserId,
                    RevieweeId = userData[0].UserId,
                    Title = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    Description = "Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.",
                    Rating = 4f,
                    Pros = "visualize seamless channels",
                    Cons = "engineer user-centric web-readiness",
                    DateCreated = DateTime.Now
                };

                Review review2 = new Review
                {
                    ReviewId = Guid.Parse("bd1effc0-c04e-4438-b44f-2c4005875f0d"),
                    ReviewerId = userData[6].UserId,
                    RevieweeId = userData[1].UserId,
                    Title = "In hac habitasse platea dictumst.",
                    Description = "Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.",
                    Rating = 1f,
                    Pros = "implement one-to-one e-business",
                    Cons = "integrate strategic initiatives",
                    DateCreated = DateTime.Now
                };

                Review review3 = new Review
                {
                    ReviewId = Guid.Parse("23ba2623-53b2-45cf-8493-a40cad3bf2f8"),
                    ReviewerId = userData[3].UserId,
                    RevieweeId = userData[0].UserId,
                    Title = "Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.",
                    Description = "Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.",
                    Rating = 3f,
                    Pros = "transform cutting-edge networks",
                    Cons = "redefine compelling web services",
                    DateCreated = DateTime.Now
                };

                ReviewDbHandler reviewDbHandler = new ReviewDbHandler(context);

                List<Review> reviewList = new List<Review>();
                reviewList.Add(review1);
                reviewList.Add(review2);
                reviewList.Add(review3);
                reviewList.Add(new Review { ReviewId = Guid.Empty });

                // Act
                var myresult = await reviewDbHandler.GetReviewsAsync();

                // Assert

                Assert.Equal(reviewList, myresult);
            }
        }

        [Fact]
        public async Task GetReviews_ReturnsList()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                var userData = new[]
            {
                new
                {
                    UserId = "110811874816751076279",
                    Email = "flambshine0@imgur.com",
                    FirstName = "Frazer",
                    LastName = "Lambshine",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2000, 5, 2),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(976) 5971010",
                    Address = "9 Golden Leaf Trail",
                    City = "Balayong",
                    Rating = 2f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "110811874816751097523",
                    Email = "sgillivrie1@utexas.edu",
                    FirstName = "Sophie",
                    LastName = "Gillivrie",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1995, 12, 5),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(894) 7687284",
                    Address = "23559 Dexter Center",
                    City = "Barobo",
                    Rating = 1f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "954853874816751076279",
                    Email = "vebbett2@odnoklassniki.ru",
                    FirstName = "Veronike",
                    LastName = "Ebbett",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1950, 2, 26),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/ff4444/ffffff",
                    Phone = "(180) 4593004",
                    Address = "056 Springview Terrace",
                    City = "Shchigry",
                    Rating = 3f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "956325694785213654589",
                    Email = "wengel3@diigo.com",
                    FirstName = "Weidar",
                    LastName = "Engel",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1970, 8, 15),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/ff4444/ffffff",
                    Phone = "(282) 5803398",
                    Address = "0 Pierstorff Trail",
                    City = "Kiambu",
                    Rating = 0.2f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "9563256947852136598520",
                    Email = "sdevaney4@histats.com",
                    FirstName = "Shayna",
                    LastName = "Devaney",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2004, 7, 18),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/cc0000/ffffff",
                    Phone = "(971) 6571942",
                    Address = "0 Anhalt Drive",
                    City = "Kudanding",
                    Rating = 4.8f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "850132544785213654589",
                    Email = "nstrewther5@nsw.gov.au",
                    FirstName = "Norean",
                    LastName = "Strewther",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2001, 3, 9),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(499) 6865801",
                    Address = "30 Cottonwood Drive",
                    City = "Teki Szlacheckie",
                    Rating = 4f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "850132958321456545893",
                    Email = "gdurn6@youku.com",
                    FirstName = "Gertrud",
                    LastName = "Durn",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1999, 9, 9),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(973) 4431447",
                    Address = "1114 Village Green Parkway",
                    City = "Muzhou",
                    Rating = 3.9f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "118269202694979787959",
                    Email = "nils.moller2000@gmail.com",
                    FirstName = "Nils",
                    LastName = "Möller",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2000, 5, 2),
                    ProfilePictureLink = "https://lh3.googleusercontent.com/a/AATXAJyaUEquS0bkZVf6rQ7bM4l8dcZ0S9_GUiRYENZKGQ=s96-c",
                    Phone = "+31642868029",
                    Address = "Heezerweg 117, 5614HC",
                    City = "Eindhoven",
                    Rating = 5f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImQzZmZiYjhhZGUwMWJiNGZhMmYyNWNmYjEwOGNjZWI4ODM0MDZkYWMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI0MDc0MDg3MTgxOTIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI0MDc0MDg3MTgxOTIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTgyNjkyMDI2OTQ5Nzk3ODc5NTkiLCJlbWFpbCI6Im5pbHMubW9sbGVyMjAwMEBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6IkhXLUx0eW14enVJNnpYYVVvMkIyQXciLCJuYW1lIjoiTmlscyIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vYS9BQVRYQUp5YVVFcXVTMGJrWlZmNnJRN2JNNGw4ZGNaMFM5X0dVaVJZRU5aS0dRPXM5Ni1jIiwiZ2l2ZW5fbmFtZSI6Ik5pbHMiLCJsb2NhbGUiOiJlbiIsImlhdCI6MTYyMTI1ODcwMCwiZXhwIjoxNjIxMjYyMzAwfQ.DjanpzA9Ws5AZC5PRnszuWcpXgyyHyPYh2B43jTmxCMEws_9ZiIzKn3xsMjuYcPfO6GwSWRdzYUHBTP0sUALCtlqtMMlRJhr6MF7cZ8Pr-hDvnEoc-mwroCdU6D_Td_CV4zzgvidCnzvvtuiY6hKzYb5mMcAtFyuiusES0HYjI3uapmeLRLmpNljWN4DFFQfLnq-RB6SkfKQYNhhXdU2xFsVyRccO5AZYYX3JWtKvCDRckyIsnxckjGnYkYlDAwnywex5WWKR3Jx6zR2dv6hb5kkx3DXy-e_CFApDXPdBtx_uB_39J5m3jvrFxq16Y1mrswuaw15k3Yt-VUCN7gjlQ"
                }
            };

                Review review1 = new Review
                {
                    ReviewId = Guid.Parse("649f29d1-f947-410e-a877-625f2c7cf3e1"),
                    ReviewerId = userData[6].UserId,
                    RevieweeId = userData[0].UserId,
                    Title = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    Description = "Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.",
                    Rating = 4f,
                    Pros = "visualize seamless channels",
                    Cons = "engineer user-centric web-readiness",
                    DateCreated = DateTime.Now
                };

                Review review2 = new Review
                {
                    ReviewId = Guid.Parse("bd1effc0-c04e-4438-b44f-2c4005875f0d"),
                    ReviewerId = userData[6].UserId,
                    RevieweeId = userData[1].UserId,
                    Title = "In hac habitasse platea dictumst.",
                    Description = "Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.",
                    Rating = 1f,
                    Pros = "implement one-to-one e-business",
                    Cons = "integrate strategic initiatives",
                    DateCreated = DateTime.Now
                };

                Review review3 = new Review
                {
                    ReviewId = Guid.Parse("23ba2623-53b2-45cf-8493-a40cad3bf2f8"),
                    ReviewerId = userData[3].UserId,
                    RevieweeId = userData[0].UserId,
                    Title = "Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.",
                    Description = "Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.",
                    Rating = 3f,
                    Pros = "transform cutting-edge networks",
                    Cons = "redefine compelling web services",
                    DateCreated = DateTime.Now
                };

                ReviewDbHandler reviewDbHandler = new ReviewDbHandler(context);

                List<Review> reviewList = new List<Review>();
                reviewList.Add(review1);
                reviewList.Add(review2);
                reviewList.Add(review3);
                reviewList.Add(new Review { ReviewId = Guid.Empty });

                // Act
                var myresult = reviewDbHandler.GetReviews();

                // Assert

                Assert.Equal(reviewList, myresult);
            }
        }

        [Fact]
        public async Task GetReviewAsync_WhenIdExists_ReturnsReview()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                ReviewDbHandler reviewDbHandler = new ReviewDbHandler(context);

                // Act
                var myresult = await reviewDbHandler.GetReviewAsync(Guid.Empty);

                // Assert
                Assert.Equal(new Review { ReviewId = Guid.Empty }, myresult);
            }
        }

        [Fact]
        public async Task GetReviewAsync_WhenIdDoesNotExist_ThrowsArgumentException()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange 
                ReviewDbHandler reviewDbHandler = new ReviewDbHandler(context);

                // Act & Assert
                await Assert.ThrowsAsync<ArgumentException>(async () => await reviewDbHandler.GetReviewAsync(Guid.NewGuid()));
            }
        }

        [Fact]
        public async Task GetReview_WhenIdExists_ReturnsReview()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                ReviewDbHandler reviewDbHandler = new ReviewDbHandler(context);

                // Act
                var myresult = reviewDbHandler.GetReview(Guid.Empty);

                // Assert
                Assert.Equal(new Review { ReviewId = Guid.Empty }, myresult);
            }
        }

        [Fact]
        public async Task GetReview_WhenIdDoesNotExist_ThrowsArgumentException()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange 
                ReviewDbHandler reviewDbHandler = new ReviewDbHandler(context);

                // Act & Assert
                Assert.Throws<ArgumentException>(() => reviewDbHandler.GetReview(Guid.NewGuid()));
            }
        }

        [Fact]
        public async Task CreateReviewAsync_AssertTrue()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                ReviewDbHandler reviewDbHandler = new ReviewDbHandler(context);
                Guid reviewID = Guid.NewGuid();
                // Act
                await reviewDbHandler.CreateReviewAsync(new Review { ReviewId = reviewID });

                // Assert
                Assert.True(await context.Reviews.ContainsAsync(new Review { ReviewId = reviewID }));
            }
        }

        [Fact]
        public async Task CreateReview_AssertTrue()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                ReviewDbHandler reviewDbHandler = new ReviewDbHandler(context);
                Guid reviewID = Guid.NewGuid();
                // Act
                reviewDbHandler.CreateReview(new Review { ReviewId = reviewID });

                //Assert
                Assert.True(context.Reviews.Contains(new Review { ReviewId = reviewID }));
            }
        }

        [Fact]
        public async Task DeleteReviewAsync_ReturnsOk()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                ReviewDbHandler reviewDbHandler = new ReviewDbHandler(context);

                // Act
                await reviewDbHandler.DeleteReviewAsync(Guid.Empty);

                // Assert
                Assert.False(await context.Reviews.ContainsAsync(new Review { ReviewId = Guid.Empty }));
            }
        }

        [Fact]
        public async Task DeleteReviewAsync_WhenIdDoesNotExist_ThrowsArgumentException()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                ReviewDbHandler reviewDbHandler = new ReviewDbHandler(context);

                // Act & Assert
                await Assert.ThrowsAsync<ArgumentException>(async () => await reviewDbHandler.DeleteReviewAsync(Guid.NewGuid()));
            }
        }

        [Fact]
        public async Task DeleteReview_ReturnsOk()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                ReviewDbHandler reviewDbHandler = new ReviewDbHandler(context);

                // Act
                reviewDbHandler.DeleteReview(Guid.Empty);

                // Assert
                Assert.False(context.Reviews.Contains(new Review { ReviewId = Guid.Empty }));
            }
        }

        [Fact]
        public async Task DeleteReview_WhenIdDoesNotExist_ThrowsArgumentException()
        {
            using (DbContext context = new DbContext(this.Options))
            {
                // Arrange
                ReviewDbHandler reviewDbHandler = new ReviewDbHandler(context);

                // Act & Assert
                await Assert.ThrowsAsync<ArgumentException>(async () => await reviewDbHandler.DeleteReviewAsync(Guid.NewGuid()));
            }
        }
    }
}