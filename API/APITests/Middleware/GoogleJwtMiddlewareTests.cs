﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Core.Db;
using Core.Models;
using Core.Wrappers;

using Google.Apis.Auth;
using Google.Apis.Util;

using Microsoft.AspNetCore.Http;

using Moq;

using Xunit;

using static Google.Apis.Auth.GoogleJsonWebSignature;

namespace API.Middleware.Tests
{
    public class GoogleJwtMiddlewareTests : IDisposable
    {
        private GoogleJwtMiddleware Middleware { get; }
        private Mock<IDictionary<object, object>> ContextItemsMock { get; }
        private Mock<HttpContext> ContextMock { get; }
        private Mock<IUserDbHandler> UserDbHandlerMock { get; }
        private Mock<RequestDelegate> NextMock { get; }
        private Mock<IValidateAsyncWrapper> ValidateAsyncWrapperMock { get; }

        // setup
        public GoogleJwtMiddlewareTests()
        {
            // Context.Items
            this.ContextItemsMock = new Mock<IDictionary<object, object>>();
            this.ContextItemsMock
                .Setup(contextItems => contextItems.Add(It.IsAny<object>(), It.IsAny<object>()));
            this.ContextItemsMock
                .Setup(contextItems => contextItems.ContainsKey(It.IsAny<object>()))
                .Returns(false);

            // HttpContext
            this.ContextMock = new Mock<HttpContext>();
            this.ContextMock
                .SetupGet(context => context.Request.Headers)
                .Returns(new HeaderDictionary()
                {
                    { "Authorization", "some valid token" }
                });
            this.ContextMock
                .SetupGet(context => context.Items)
                .Returns(this.ContextItemsMock.Object);

            // UserDbHandler
            this.UserDbHandlerMock = new Mock<IUserDbHandler>();
            this.UserDbHandlerMock
                .Setup(dbHandler => dbHandler.GetUserAsync(It.IsAny<string>()))
                .ReturnsAsync(new User { UserId = "000000000000000000000" });
            this.UserDbHandlerMock
                .Setup(dbHandler => dbHandler.UpdateUserAsync(It.IsAny<User>(), It.IsAny<bool>()))
                .Returns(Task.CompletedTask);

            // RequestDelegate
            this.NextMock = new Mock<RequestDelegate>();

            // ValidateAsyncWrapper
            this.ValidateAsyncWrapperMock = new Mock<IValidateAsyncWrapper>();
            this.ValidateAsyncWrapperMock
                .Setup(validateAsyncWrapper => validateAsyncWrapper.ValidateAsync(It.IsAny<string>(), It.IsAny<IClock>(), It.IsAny<bool>()))
                .ReturnsAsync(new Payload());

            this.Middleware = new GoogleJwtMiddleware(NextMock.Object);
        }

        // breakdown
        public void Dispose()
        {

        }

        [Fact]
        public async Task InvokeAsync_CallsNext()
        {
            // Act
            await this.Middleware.InvokeAsync(this.ContextMock.Object, this.UserDbHandlerMock.Object, this.ValidateAsyncWrapperMock.Object);

            // Assert
            this.NextMock.Verify(next => next(this.ContextMock.Object), Times.Once);
        }

        [Fact]
        public async Task InvokeAsync_ValidatesToken()
        {
            // Act
            await this.Middleware.InvokeAsync(this.ContextMock.Object, this.UserDbHandlerMock.Object, this.ValidateAsyncWrapperMock.Object);

            // Assert
            this.ValidateAsyncWrapperMock.Verify(validateAsyncWrapper => validateAsyncWrapper.ValidateAsync(It.IsAny<string>(), It.IsAny<IClock>(), It.IsAny<bool>()), Times.Once);
        }

        [Fact]
        public async Task InvokeAsync_SetsUserContextItem_WhenTokenIsValid()
        {
            // Act
            await this.Middleware.InvokeAsync(this.ContextMock.Object, this.UserDbHandlerMock.Object, this.ValidateAsyncWrapperMock.Object);

            // Assert
            this.ContextItemsMock.Verify(contextitems => contextitems.Add(It.IsAny<object>(), It.IsAny<object>()), Times.Once);
        }

        [Fact]
        public async Task InvokeAsync_UpdatesUserLastLogin_WhenTokenIsValid()
        {
            // Act
            await this.Middleware.InvokeAsync(this.ContextMock.Object, this.UserDbHandlerMock.Object, this.ValidateAsyncWrapperMock.Object);

            // Assert
            this.UserDbHandlerMock.Verify(dbHandler => dbHandler.GetUserAsync(It.IsAny<string>()), Times.Once);
            this.UserDbHandlerMock.Verify(dbHandler => dbHandler.UpdateUserAsync(It.IsAny<User>(), It.IsAny<bool>()), Times.Once);
        }

        [Fact]
        public async Task InvokeAsync_DoesNotSetContextItemUserOrUpdatesUser_WhenTokenIsInvalid()
        {
            // Arrange
            this.ValidateAsyncWrapperMock
                .Setup(validateAsyncWrapper => validateAsyncWrapper.ValidateAsync(It.IsAny<string>(), It.IsAny<IClock>(), It.IsAny<bool>()))
                .ThrowsAsync(new InvalidJwtException("Invalid JWT."));

            // Act
            await this.Middleware.InvokeAsync(this.ContextMock.Object, this.UserDbHandlerMock.Object, this.ValidateAsyncWrapperMock.Object);

            // Assert
            this.ContextMock.Verify(context => context.Items.Add(It.IsAny<string>(), It.IsAny<User>()), Times.Never);
            this.UserDbHandlerMock.Verify(dbHandler => dbHandler.GetUserAsync(It.IsAny<string>()), Times.Never);
            this.UserDbHandlerMock.Verify(dbHandler => dbHandler.UpdateUserAsync(It.IsAny<User>(), It.IsAny<bool>()), Times.Never);
        }
    }
}
