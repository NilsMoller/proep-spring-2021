﻿
using System;
using System.Threading.Tasks;
using API.Controllers;
using Xunit;

namespace API.Controllers.Tests
{
    public class PingControllerTests : IDisposable
    {

        public PingController _controller;


        // setup
        public PingControllerTests()
        {

        }

        // breakdown
        public void Dispose()
        {

        }

        [Fact]
        public async Task Ping_Return_Pong()
        {
            _controller = new PingController();

            var myresult = await this._controller.Ping();

            Assert.Equal("Pong", myresult);
        }
    }
}