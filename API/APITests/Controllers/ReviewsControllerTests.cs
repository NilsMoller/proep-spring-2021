﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Collections.Generic;
using API.Attributes;
using API.Controllers;

using Core.Db;
using Core.Models;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Moq;

using Xunit;
using API.Helpers;

namespace APITests.Controllers
{
    public class ReviewsControllerTests : IDisposable

    {
        public Mock<IReviewDbHandler> _reviewMock;

        public Mock<IUserDbHandler> _userDbHandlerMock;

        ReviewsController _controller;

        // setup
        public ReviewsControllerTests()
        {
            _reviewMock = new Mock<IReviewDbHandler>();
            _userDbHandlerMock = new Mock<IUserDbHandler>();
        }

        // breakdown
        public void Dispose()
        {

        }

        [Fact]
        public async Task GetAllReviews_OK()
        {
            //Arrange
            this._reviewMock.Setup(k => k.GetReviewsAsync());
            this._controller = new ReviewsController(this._reviewMock.Object,new Mock<IUserDbHandler>().Object
                , new Mock<IBookingDbHandler>().Object, new Mock<IJobDbHandler>().Object);

            //Act
            var myresult = await this._controller.GetReviews();

            //Assert
            Assert.IsType<OkObjectResult>(myresult.Result);
        }

        [Fact]
        public async Task GetReviewByID_ReturnOK()
        {
            //Setup
            Guid reviewID = Guid.NewGuid();

            // Arrange
            this._reviewMock.Setup(k => k.GetReviewAsync(reviewID));
            this._controller = new ReviewsController(this._reviewMock.Object, new Mock<IUserDbHandler>().Object
                , new Mock<IBookingDbHandler>().Object, new Mock<IJobDbHandler>().Object);

            // Act
            var myresult = await this._controller.GetReviewById(reviewID);

            //Assert
            Assert.IsType<OkObjectResult>(myresult.Result);
        }

        [Fact]
        public async Task GetReviewByID_ReturnNotFound()
        {
            //Setup
            Guid reviewID = Guid.NewGuid();

            // Arrange
            this._reviewMock.Setup(k => k.GetReviewAsync(reviewID)).ThrowsAsync(new ArgumentException());
            this._controller = new ReviewsController(this._reviewMock.Object, new Mock<IUserDbHandler>().Object
                , new Mock<IBookingDbHandler>().Object, new Mock<IJobDbHandler>().Object);

            // Act
            var myresult = await this._controller.GetReviewById(reviewID);

            //Assert
            Assert.IsType<NotFoundObjectResult>(myresult.Result);
        }

        [Fact]
        public async Task GetReview_ReturnsReview_WhenMatchingIDExists()
        {
            //Arrange
            Guid reviewId = Guid.Parse("649f29d1-f947-410e-a877-625f2c7cf3e1");
            string reviewTitle = "Lorem";

            Review reviewDto = new Review
            {
                ReviewId = reviewId,
                Title = reviewTitle
            };
            this._reviewMock.Setup(p => p.GetReviewAsync(reviewId)).ReturnsAsync(reviewDto);
            this._controller = new ReviewsController(this._reviewMock.Object, new Mock<IUserDbHandler>().Object, new Mock<IBookingDbHandler>().Object, new Mock<IJobDbHandler>().Object);

            //Act
            ActionResult<Review> response = await this._controller.GetReviewById(reviewId);

            //Assert
            OkObjectResult result = response.Result as OkObjectResult;
            Review reviewResponse = result.Value as Review;

            Assert.Equal(reviewDto, reviewResponse);
        }

        [Fact]
        public async Task DeleteReview_ReturnsOkResult()
        {
            //Arrange
            Guid reviewId = Guid.NewGuid();
            string userId = "110811874816751097523";

            this._reviewMock.Setup(p => p.GetReviewAsync(reviewId)).ReturnsAsync(new Review { ReviewId = reviewId, ReviewerId = userId });
            this._controller = new ReviewsController(this._reviewMock.Object, new Mock<IUserDbHandler>().Object, new Mock<IBookingDbHandler>().Object, new Mock<IJobDbHandler>().Object);

            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = userId });

            //Act
            ActionResult okResponse = await this._controller.DeleteReview(reviewId);

            //Assert
            Assert.IsType<OkResult>(okResponse);
        }

        [Fact]
        public async Task DeleteReview_WhenWrongHttpContext_ReturnsUnauthorizedResult()
        {
            //Arrange
            Guid reviewId = Guid.NewGuid();
            string userId = "118269202694979787959";

            this._reviewMock.Setup(p => p.GetReviewAsync(reviewId)).ReturnsAsync(new Review { ReviewerId = userId });
            this._controller = new ReviewsController(this._reviewMock.Object, new Mock<IUserDbHandler>().Object, new Mock<IBookingDbHandler>().Object, new Mock<IJobDbHandler>().Object);

            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = "118269202694979787000" });

            //Act
            ActionResult okResponse = await this._controller.DeleteReview(reviewId);

            //Assert
            Assert.IsType<UnauthorizedResult>(okResponse);
        }

        [Fact]
        public async Task CreateReview_MustHaveAuthorizeAttribute()
        {
            // Arrange
            MethodBase method = typeof(ReviewsController).GetMethod(nameof(this._controller.CreateReview));

            // Act
            object[] attributes = method.GetCustomAttributes(typeof(AuthorizeAttribute), true);

            // Assert
            Assert.Single(attributes);
        }

        [Fact]
        public async Task DeleteReview_MustHaveAuthorizeAttribute()
        {
            // Arrange
            MethodBase method = typeof(ReviewsController).GetMethod(nameof(this._controller.DeleteReview));

            // Act & Assert
            object[] attributes = method.GetCustomAttributes(typeof(AuthorizeAttribute), true);

            // Assert
            Assert.Single(attributes);
        }
        
        [Fact]
        public async Task GetReviewsByReviewer_ReturnsOK()
        {
            //Setup
            string reviewerID = GoogleUserIdGenerator.GenerateRandomGoogleUserId();
            //string reviewerID = "110811874816751097523";

            // Arrange
            this._reviewMock.Setup(k => k.GetReviewsAsync()).ReturnsAsync(new List<Review>());
            this._controller = new ReviewsController(this._reviewMock.Object, new Mock<IUserDbHandler>().Object
                , new Mock<IBookingDbHandler>().Object, new Mock<IJobDbHandler>().Object);

            // Act
            var myresult = await this._controller.GetReviewsByReviewer(reviewerID);

            //Assert
            Assert.IsType<OkObjectResult>(myresult.Result);

        }

        [Fact]
        public async Task GetReviewsByReviewer_ReturnsNotFound()
        {
            //Setup
            string reviewerID = GoogleUserIdGenerator.GenerateRandomGoogleUserId();

            // Arrange
            this._userDbHandlerMock.Setup(k => k.GetUserAsync(reviewerID)).ThrowsAsync(new ArgumentException());
            this._controller = new ReviewsController(this._reviewMock.Object, new Mock<IUserDbHandler>().Object
                , new Mock<IBookingDbHandler>().Object, new Mock<IJobDbHandler>().Object);

            // Act
            var myresult = await this._controller.GetReviewsByReviewer(reviewerID);

            //Assert
            Assert.IsType<NotFoundObjectResult>(myresult.Result);
        }

        [Fact]
        public async Task GetReviewsByReviewerAndReviewee_ReturnsOK()
        {
            //Setup
            string reviewerID = GoogleUserIdGenerator.GenerateRandomGoogleUserId();
            string revieweeID = GoogleUserIdGenerator.GenerateRandomGoogleUserId();
            // Arrange
            this._reviewMock.Setup(k => k.GetReviewsAsync()).ReturnsAsync(new List<Review>());
            this._controller = new ReviewsController(this._reviewMock.Object, new Mock<IUserDbHandler>().Object
                , new Mock<IBookingDbHandler>().Object, new Mock<IJobDbHandler>().Object);

            // Act
            var myresult = await this._controller.GetReviewsByReviewerAndReviewee(reviewerID, revieweeID);

            //Assert
            Assert.IsType<OkObjectResult>(myresult.Result);
        }

        [Fact]
        public async Task GetReviewsByReviewerAndReviewee_ReturnsNotFound_Reviewer()
        {
            //Setup
            string reviewerID = GoogleUserIdGenerator.GenerateRandomGoogleUserId();
            string revieweeID = GoogleUserIdGenerator.GenerateRandomGoogleUserId();

            // Arrange
            //this._reviewMock.Setup(k => k.GetReviewsAsync()).ThrowsAsync(new ArgumentException);
            this._userDbHandlerMock.Setup(k => k.GetUserAsync(reviewerID)).ThrowsAsync(new ArgumentException());

            this._controller = new ReviewsController(this._reviewMock.Object, new Mock<IUserDbHandler>().Object
                , new Mock<IBookingDbHandler>().Object, new Mock<IJobDbHandler>().Object);

            // Act
            var myresult = await this._controller.GetReviewsByReviewerAndReviewee(reviewerID, revieweeID);

            //Assert
            Assert.IsType<NotFoundObjectResult>(myresult.Result);
        }

        [Fact]
        public async Task GetReviewsByReviewerAndReviewee_ReturnsNotFound_Reviewee()
        {
            //Setup
            string reviewerID = GoogleUserIdGenerator.GenerateRandomGoogleUserId();
            string revieweeID = GoogleUserIdGenerator.GenerateRandomGoogleUserId();

            // Arrange
            //this._reviewMock.Setup(k => k.GetReviewsAsync()).ThrowsAsync(new ArgumentException);
            this._userDbHandlerMock.Setup(k => k.GetUserAsync(revieweeID)).ThrowsAsync(new ArgumentException());

            this._controller = new ReviewsController(this._reviewMock.Object, new Mock<IUserDbHandler>().Object
                , new Mock<IBookingDbHandler>().Object, new Mock<IJobDbHandler>().Object);

            // Act
            var myresult = await this._controller.GetReviewsByReviewerAndReviewee(reviewerID, revieweeID);

            //Assert
            Assert.IsType<NotFoundObjectResult>(myresult.Result);
        }
    }
}
