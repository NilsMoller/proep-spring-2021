﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

using API.Attributes;
using API.Controllers;

using Core.Db;
using Core.Models;
using API.Helpers;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Moq;

using Xunit;

namespace APITests.Controllers
{
    public class BookingsControllerTests : IDisposable
    {
        public Mock<IBookingDbHandler> _bookingMock;
        BookingsController _controller;

        // setup
        public BookingsControllerTests()
        {
            _bookingMock = new Mock<IBookingDbHandler>();
        }

        // breakdown
        public void Dispose()
        {

        }

        #region Get

        [Fact]
        public async Task GetBookings_ReturnsOkResult()
        {
            //Arrange
            this._bookingMock.Setup(p => p.GetBookingsAsync());
            this._controller = new BookingsController(this._bookingMock.Object, new Mock<IJobDbHandler>().Object, new Mock<IUserDbHandler>().Object);

            // Act
            var okResult = await this._controller.GetBookings();

            // Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public async Task GetBooking_ReturnsBooking_WhenMatchingIDExists()
        {
            //Arrange
            Guid bookingId = Guid.NewGuid();
            Guid jobID = Guid.NewGuid();
            string consumerID = "110811874816751097523";

            Booking bookingDto = new Booking
            {
                BookingId = bookingId,
                JobId = jobID,
                ConsumerId = consumerID,
                Address = "Rachelsmolen 1"
            };
            this._bookingMock.Setup(p => p.GetBookingAsync(bookingId)).ReturnsAsync(bookingDto);
            this._controller = new BookingsController(this._bookingMock.Object, new Mock<IJobDbHandler>().Object, new Mock<IUserDbHandler>().Object);

            //Act
            ActionResult<Booking> response = await this._controller.GetBooking(bookingId);

            //Assert
            OkObjectResult result = response.Result as OkObjectResult;
            Booking bookingResponse = result.Value as Booking;

            Assert.Equal(bookingDto, bookingResponse);
        }

        [Fact]
        public async Task GetBookingWithId_ReturnsRightItem()
        {
            //Arrange
            Guid bookingId = Guid.Parse("8303c61a-7f69-479d-a873-4494ad26d198");
            string userId = "110811874816751097523";

            this._bookingMock.Setup(p => p.GetBookingAsync(bookingId)).ReturnsAsync(new Booking { BookingId = bookingId, ConsumerId = userId });
            this._controller = new BookingsController(this._bookingMock.Object, new Mock<IJobDbHandler>().Object, new Mock<IUserDbHandler>().Object);

            // Act
            var result = await this._controller.GetBooking(bookingId);
            var okResult = result.Result as OkObjectResult;

            // Assert
            Assert.IsType<Booking>(okResult.Value);
            Assert.Equal(bookingId, (okResult.Value as Booking).BookingId);
        }

        [Fact]
        public async Task GetBooking_ReturnsNotFound()
        {
            //Arrange
            Guid bookingId = Guid.NewGuid();

            this._bookingMock.Setup(p => p.GetBookingAsync(bookingId)).ThrowsAsync(new ArgumentException());
            this._controller = new BookingsController(this._bookingMock.Object, new Mock<IJobDbHandler>().Object, new Mock<IUserDbHandler>().Object);

            // Act
            var response = await this._controller.GetBooking(bookingId);

            // Assert
            Assert.IsType<NotFoundObjectResult>(response.Result);
        }

        [Fact]
        public async Task GetBookingsForJob_ReturnsOkResult()
        {
            //Arrange
            Guid jobId = Guid.Parse("debe96be-f90d-4677-8203-aba156b5ef74");
            Guid bookingId = Guid.Parse("ce1c8aa8-f0fd-473a-8ffc-c664d22f6a45");

            Mock<IJobDbHandler> jMock = new Mock<IJobDbHandler>();
            jMock.Setup(k => k.GetJobAsync(It.IsAny<Guid>())).ReturnsAsync(new Job());

            this._bookingMock.Setup(p => p.GetBookingsAsync()).ReturnsAsync(new List<Booking>());
            this._controller = new BookingsController(this._bookingMock.Object, jMock.Object, new Mock<IUserDbHandler>().Object);

            // Act
            var Result = await this._controller.GetBookingsForJob(jobId);

            // Assert
            Assert.IsType<OkObjectResult>(Result.Result);
            Assert.IsType<ActionResult<List<Booking>>>(Result);
        }

        [Fact]
        public async Task GetBookingsForJob_WhenJobNotExist_ReturnsBadRequest()
        {
            //Arrange
            Guid jobId = Guid.NewGuid();

            this._bookingMock.Setup(p => p.GetBookingsAsync()).ThrowsAsync(new ArgumentException());
            this._controller = new BookingsController(this._bookingMock.Object, new Mock<IJobDbHandler>().Object, new Mock<IUserDbHandler>().Object);

            // Act
            var Result = await this._controller.GetBookingsForJob(jobId);

            // Assert
            Assert.IsType<BadRequestObjectResult>(Result.Result);
        }

        #endregion

        #region Create

        [Fact]
        public async Task CreateBooking_ReturnsCreatedAtAction()
        {
            //Arrange
            Guid bookingId = Guid.NewGuid();
            Guid jobID = Guid.NewGuid();
            string consumerID = "850132958321456545893";

            Booking bookingDto = new Booking
            {
                BookingId = bookingId,
                JobId = jobID,
                ConsumerId = consumerID,
                Address = "Rachelsmolen 1",
                City = "Eindhoven",
                BookingDate = DateTime.Now
            };

            Mock<IJobDbHandler> jMock = new Mock<IJobDbHandler>();
            jMock.Setup(k => k.GetJobAsync(bookingDto.JobId)).ReturnsAsync(new Job { ProviderId = "000032958321456540000" });

            Mock<IUserDbHandler> uMock = new Mock<IUserDbHandler>();
            uMock.Setup(k => k.GetUserAsync(bookingDto.ConsumerId)).ReturnsAsync(new User());

            this._bookingMock.Setup(p => p.GetBookingsAsync()).ReturnsAsync(new List<Booking>());

            this._controller = new BookingsController(this._bookingMock.Object, jMock.Object, uMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = consumerID });

            //Act
            var response = await this._controller.CreateBooking(bookingDto);

            //Assert

            Assert.IsType<CreatedAtActionResult>(response.Result);
        }

        [Fact]
        public async Task CreateBooking_ReturnsUnauthorized()
        {
            //Arrange
            Guid bookingId = Guid.NewGuid();
            Guid jobID = Guid.NewGuid();
            string consumerID = "850132958321456545893";

            Booking bookingDto = new Booking
            {
                BookingId = bookingId,
                JobId = jobID,
                ConsumerId = consumerID,
                Address = "Rachelsmolen 1",
                City = "Eindhoven",
                BookingDate = DateTime.Now
            };

            Mock<IJobDbHandler> jMock = new Mock<IJobDbHandler>();
            jMock.Setup(k => k.GetJobAsync(bookingDto.JobId)).ReturnsAsync(new Job { ProviderId = GoogleUserIdGenerator.GenerateRandomGoogleUserId() });

            Mock<IUserDbHandler> uMock = new Mock<IUserDbHandler>();
            uMock.Setup(k => k.GetUserAsync(bookingDto.ConsumerId)).ReturnsAsync(new User());

            this._bookingMock.Setup(p => p.GetBookingsAsync()).ReturnsAsync(new List<Booking>());

            this._controller = new BookingsController(this._bookingMock.Object, jMock.Object, uMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = GoogleUserIdGenerator.GenerateRandomGoogleUserId() });

            //Act
            var response = await this._controller.CreateBooking(bookingDto);

            //Assert

            Assert.IsType<UnauthorizedResult>(response.Result);
        }

        [Fact]
        public async Task CreateBooking_WhenConsumerNotExists_ReturnsBadRequest()
        {
            //Arrange
            Guid bookingId = Guid.NewGuid();
            Guid jobID = Guid.NewGuid();
            string consumerID = "850132958321456545893";

            Booking bookingDto = new Booking
            {
                BookingId = bookingId,
                JobId = jobID,
                ConsumerId = consumerID,
                Address = "Rachelsmolen 1",
                City = "Eindhoven",
                BookingDate = DateTime.Now
            };

            Mock<IJobDbHandler> jMock = new Mock<IJobDbHandler>();
            jMock.Setup(k => k.GetJobAsync(bookingDto.JobId)).ReturnsAsync(new Job { ProviderId = "000032958321456540000" });

            Mock<IUserDbHandler> uMock = new Mock<IUserDbHandler>();
            uMock.Setup(k => k.GetUserAsync(bookingDto.ConsumerId)).ThrowsAsync(new ArgumentException());

            this._bookingMock.Setup(p => p.GetBookingsAsync()).ReturnsAsync(new List<Booking>());

            this._controller = new BookingsController(this._bookingMock.Object, jMock.Object, uMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = consumerID });

            //Act
            var response = await this._controller.CreateBooking(bookingDto);

            //Assert

            Assert.IsType<BadRequestObjectResult>(response.Result);
        }

        [Fact]
        public async Task CreateBooking_WhenJobNotExists_ReturnsBadRequest()
        {
            //Arrange
            Guid bookingId = Guid.NewGuid();
            Guid jobID = Guid.NewGuid();
            string consumerID = "850132958321456545893";

            Booking bookingDto = new Booking
            {
                BookingId = bookingId,
                JobId = jobID,
                ConsumerId = consumerID,
                Address = "Rachelsmolen 1",
                City = "Eindhoven",
                BookingDate = DateTime.Now
            };

            Mock<IJobDbHandler> jMock = new Mock<IJobDbHandler>();
            jMock.Setup(k => k.GetJobAsync(bookingDto.JobId)).ThrowsAsync(new ArgumentException());

            Mock<IUserDbHandler> uMock = new Mock<IUserDbHandler>();
            uMock.Setup(k => k.GetUserAsync(bookingDto.ConsumerId)).ReturnsAsync(new User());

            this._bookingMock.Setup(p => p.GetBookingsAsync()).ReturnsAsync(new List<Booking>());

            this._controller = new BookingsController(this._bookingMock.Object, jMock.Object, uMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = consumerID });

            //Act
            var response = await this._controller.CreateBooking(bookingDto);

            //Assert

            Assert.IsType<BadRequestObjectResult>(response.Result);
        }

        [Fact]
        public async Task CreateBooking_WhenConsumerIsAlsoProvider_ReturnsBadRequest()
        {
            //Arrange
            Guid bookingId = Guid.NewGuid();
            Guid jobID = Guid.NewGuid();
            string consumerID = "850132958321456545893";

            Booking bookingDto = new Booking
            {
                BookingId = bookingId,
                JobId = jobID,
                ConsumerId = consumerID,
                Address = "Rachelsmolen 1",
                City = "Eindhoven",
                BookingDate = DateTime.Now
            };

            Mock<IJobDbHandler> jMock = new Mock<IJobDbHandler>();
            jMock.Setup(k => k.GetJobAsync(bookingDto.JobId)).ReturnsAsync(new Job { ProviderId = consumerID });

            Mock<IUserDbHandler> uMock = new Mock<IUserDbHandler>();
            uMock.Setup(k => k.GetUserAsync(bookingDto.ConsumerId)).ReturnsAsync(new User());

            this._bookingMock.Setup(p => p.GetBookingsAsync()).ReturnsAsync(new List<Booking>());

            this._controller = new BookingsController(this._bookingMock.Object, jMock.Object, uMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = consumerID });

            //Act
            var response = await this._controller.CreateBooking(bookingDto);

            //Assert

            Assert.IsType<BadRequestObjectResult>(response.Result);
        }

        [Fact]
        public async Task CreateBooking_WhenJobAlreadyHasBookingOnGivenDate_ReturnsBadRequest()
        {
            //Arrange
            Guid bookingId = Guid.NewGuid();
            Guid jobID = Guid.NewGuid();
            string consumerID = "850132958321456545893";

            Booking bookingDto = new Booking
            {
                BookingId = bookingId,
                JobId = jobID,
                ConsumerId = consumerID,
                Address = "Rachelsmolen 1",
                City = "Eindhoven",
                BookingDate = DateTime.Now
            };

            Mock<IJobDbHandler> jMock = new Mock<IJobDbHandler>();
            jMock.Setup(k => k.GetJobAsync(bookingDto.JobId)).ReturnsAsync(new Job { ProviderId = "000032958321456540000"});

            Mock<IUserDbHandler> uMock = new Mock<IUserDbHandler>();
            uMock.Setup(k => k.GetUserAsync(bookingDto.ConsumerId)).ReturnsAsync(new User());

            this._bookingMock.Setup(p => p.GetBookingsAsync()).ReturnsAsync(new List<Booking> { new Booking { JobId = bookingDto.JobId , BookingDate = bookingDto.BookingDate} });

            this._controller = new BookingsController(this._bookingMock.Object, jMock.Object, uMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = consumerID });

            //Act
            var response = await this._controller.CreateBooking(bookingDto);

            //Assert

            Assert.IsType<BadRequestObjectResult>(response.Result);
        }
        #endregion

        #region Update

        [Fact]
        public async Task UpdateBooking_ReturnsNoContent()
        {
            //Arrange
            Guid bookingId = Guid.NewGuid();
            Guid jobID = Guid.NewGuid();
            string consumerID = "850132958321456545893";

            Booking bookingDto = new Booking
            {
                BookingId = bookingId,
                JobId = jobID,
                ConsumerId = consumerID,
                Address = "Rachelsmolen 1",
                City = "Eindhoven",
                BookingDate = DateTime.Now
            };

            Mock<IJobDbHandler> jMock = new Mock<IJobDbHandler>();
            jMock.Setup(k => k.GetJobAsync(bookingDto.JobId)).ReturnsAsync(new Job());

            Mock<IUserDbHandler> uMock = new Mock<IUserDbHandler>();
            uMock.Setup(k => k.GetUserAsync(bookingDto.ConsumerId)).ReturnsAsync(new User());

            this._bookingMock.Setup(p => p.GetBookingAsync(bookingDto.BookingId))
                .ReturnsAsync(new Booking { BookingId = bookingDto.BookingId, JobId = bookingDto.JobId, 
                    ConsumerId = bookingDto.ConsumerId, DateCreated = bookingDto.DateCreated });

            this._controller = new BookingsController(this._bookingMock.Object, jMock.Object, uMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = consumerID });

            //Act
            var response = await this._controller.UpdateBooking(bookingDto.BookingId, bookingDto);

            //Assert

            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public async Task UpdateBooking_ReturnsUnauthorized()
        {
            //Arrange
            Guid bookingId = Guid.NewGuid();
            Guid jobID = Guid.NewGuid();
            string consumerID = "850132958321456545893";

            Booking bookingDto = new Booking
            {
                BookingId = bookingId,
                JobId = jobID,
                ConsumerId = consumerID,
                Address = "Rachelsmolen 1",
                City = "Eindhoven",
                BookingDate = DateTime.Now
            };

            Mock<IJobDbHandler> jMock = new Mock<IJobDbHandler>();
            jMock.Setup(k => k.GetJobAsync(bookingDto.JobId)).ReturnsAsync(new Job());

            Mock<IUserDbHandler> uMock = new Mock<IUserDbHandler>();
            uMock.Setup(k => k.GetUserAsync(bookingDto.ConsumerId)).ReturnsAsync(new User());

            this._bookingMock.Setup(p => p.GetBookingAsync(bookingDto.BookingId))
                .ReturnsAsync(new Booking
                {
                    BookingId = bookingDto.BookingId,
                    JobId = bookingDto.JobId,
                    ConsumerId = bookingDto.ConsumerId,
                    DateCreated = bookingDto.DateCreated
                });

            this._controller = new BookingsController(this._bookingMock.Object, jMock.Object, uMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = GoogleUserIdGenerator.GenerateRandomGoogleUserId()});

            //Act
            var response = await this._controller.UpdateBooking(bookingDto.BookingId, bookingDto);

            //Assert

            Assert.IsType<UnauthorizedResult>(response);
        }

        [Fact]
        public async Task UpdateBooking_WhenIdNotMatchBookingId_ReturnBadRequest()
        {
            //Arrange
            Guid bookingId = Guid.NewGuid();
            Guid jobID = Guid.NewGuid();
            string consumerID = "850132958321456545893";

            Booking bookingDto = new Booking
            {
                BookingId = bookingId,
                JobId = jobID,
                ConsumerId = consumerID,
                Address = "Rachelsmolen 1",
                City = "Eindhoven",
                BookingDate = DateTime.Now
            };

            Mock<IJobDbHandler> jMock = new Mock<IJobDbHandler>();
            jMock.Setup(k => k.GetJobAsync(bookingDto.JobId)).ReturnsAsync(new Job());

            Mock<IUserDbHandler> uMock = new Mock<IUserDbHandler>();
            uMock.Setup(k => k.GetUserAsync(bookingDto.ConsumerId)).ReturnsAsync(new User());

            this._bookingMock.Setup(p => p.GetBookingAsync(bookingDto.BookingId))
                .ReturnsAsync(new Booking
                {
                    BookingId = bookingDto.BookingId,
                    JobId = bookingDto.JobId,
                    ConsumerId = bookingDto.ConsumerId,
                    DateCreated = bookingDto.DateCreated
                });

            this._controller = new BookingsController(this._bookingMock.Object, jMock.Object, uMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = consumerID });

            //Act
            var response = await this._controller.UpdateBooking(Guid.NewGuid(), bookingDto);

            //Assert

            Assert.IsType<BadRequestObjectResult>(response);
        }

        [Fact]
        public async Task UpdateBooking_WhenBookingNotExist_ReturnNotFound()
        {
            //Arrange
            Guid bookingId = Guid.NewGuid();
            Guid jobID = Guid.NewGuid();
            string consumerID = "850132958321456545893";

            Booking bookingDto = new Booking
            {
                BookingId = bookingId,
                JobId = jobID,
                ConsumerId = consumerID,
                Address = "Rachelsmolen 1",
                City = "Eindhoven",
                BookingDate = DateTime.Now
            };

            Mock<IJobDbHandler> jMock = new Mock<IJobDbHandler>();
            jMock.Setup(k => k.GetJobAsync(bookingDto.JobId)).ReturnsAsync(new Job());

            Mock<IUserDbHandler> uMock = new Mock<IUserDbHandler>();
            uMock.Setup(k => k.GetUserAsync(bookingDto.ConsumerId)).ReturnsAsync(new User());

            this._bookingMock.Setup(p => p.GetBookingAsync(bookingDto.BookingId)).ThrowsAsync(new ArgumentException());

            this._controller = new BookingsController(this._bookingMock.Object, jMock.Object, uMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = consumerID });

            //Act
            var response = await this._controller.UpdateBooking(bookingDto.BookingId, bookingDto);

            //Assert

            Assert.IsType<NotFoundObjectResult>(response);
        }

        [Fact]
        public async Task UpdateBooking_WhenBookingIdDoesNotMatchAnyExistingId_ReturnsBadRequest()
        {
            //Arrange
            Guid bookingId = Guid.NewGuid();
            Guid jobID = Guid.NewGuid();
            string consumerID = "850132958321456545893";

            Booking bookingDto = new Booking
            {
                BookingId = bookingId,
                JobId = jobID,
                ConsumerId = consumerID,
                Address = "Rachelsmolen 1",
                City = "Eindhoven",
                BookingDate = DateTime.Now
            };

            Mock<IJobDbHandler> jMock = new Mock<IJobDbHandler>();
            jMock.Setup(k => k.GetJobAsync(bookingDto.JobId)).ReturnsAsync(new Job());

            Mock<IUserDbHandler> uMock = new Mock<IUserDbHandler>();
            uMock.Setup(k => k.GetUserAsync(bookingDto.ConsumerId)).ReturnsAsync(new User());

            this._bookingMock.Setup(p => p.GetBookingAsync(bookingDto.BookingId))
                .ReturnsAsync(new Booking
                {
                    BookingId = Guid.NewGuid(),
                    JobId = bookingDto.JobId,
                    ConsumerId = bookingDto.ConsumerId,
                    DateCreated = bookingDto.DateCreated
                });

            this._controller = new BookingsController(this._bookingMock.Object, jMock.Object, uMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = consumerID });

            //Act
            var response = await this._controller.UpdateBooking(bookingDto.BookingId, bookingDto);

            //Assert

            Assert.IsType<BadRequestObjectResult>(response);
        }

        [Fact]
        public async Task UpdateBooking_WhenJobIdDoesNotMatchAnyExistingId_ReturnsBadRequest()
        {
            //Arrange
            Guid bookingId = Guid.NewGuid();
            Guid jobID = Guid.NewGuid();
            string consumerID = "850132958321456545893";

            Booking bookingDto = new Booking
            {
                BookingId = bookingId,
                JobId = jobID,
                ConsumerId = consumerID,
                Address = "Rachelsmolen 1",
                City = "Eindhoven",
                BookingDate = DateTime.Now
            };

            Mock<IJobDbHandler> jMock = new Mock<IJobDbHandler>();
            jMock.Setup(k => k.GetJobAsync(bookingDto.JobId)).ReturnsAsync(new Job());

            Mock<IUserDbHandler> uMock = new Mock<IUserDbHandler>();
            uMock.Setup(k => k.GetUserAsync(bookingDto.ConsumerId)).ReturnsAsync(new User());

            this._bookingMock.Setup(p => p.GetBookingAsync(bookingDto.BookingId))
                .ReturnsAsync(new Booking
                {
                    BookingId = bookingDto.BookingId,
                    JobId = Guid.NewGuid(),
                    ConsumerId = bookingDto.ConsumerId,
                    DateCreated = bookingDto.DateCreated
                });

            this._controller = new BookingsController(this._bookingMock.Object, jMock.Object, uMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = consumerID });

            //Act
            var response = await this._controller.UpdateBooking(bookingDto.BookingId, bookingDto);

            //Assert

            Assert.IsType<BadRequestObjectResult>(response);
        }

        [Fact]
        public async Task UpdateBooking_WhenConsumerIdDoesNotMatchAnyExistingId_ReturnsBadRequest()
        {
            //Arrange
            Guid bookingId = Guid.NewGuid();
            Guid jobID = Guid.NewGuid();
            string consumerID = "850132958321456545893";

            Booking bookingDto = new Booking
            {
                BookingId = bookingId,
                JobId = jobID,
                ConsumerId = consumerID,
                Address = "Rachelsmolen 1",
                City = "Eindhoven",
                BookingDate = DateTime.Now
            };

            Mock<IJobDbHandler> jMock = new Mock<IJobDbHandler>();
            jMock.Setup(k => k.GetJobAsync(bookingDto.JobId)).ReturnsAsync(new Job());

            Mock<IUserDbHandler> uMock = new Mock<IUserDbHandler>();
            uMock.Setup(k => k.GetUserAsync(bookingDto.ConsumerId)).ReturnsAsync(new User());

            this._bookingMock.Setup(p => p.GetBookingAsync(bookingDto.BookingId))
                .ReturnsAsync(new Booking
                {
                    BookingId = bookingDto.BookingId,
                    JobId = bookingDto.JobId,
                    ConsumerId = GoogleUserIdGenerator.GenerateRandomGoogleUserId(),
                    DateCreated = bookingDto.DateCreated
                });

            this._controller = new BookingsController(this._bookingMock.Object, jMock.Object, uMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = consumerID });

            //Act
            var response = await this._controller.UpdateBooking(bookingDto.BookingId, bookingDto);

            //Assert

            Assert.IsType<BadRequestObjectResult>(response);
        }

        [Fact]
        public async Task UpdateBooking_WhenDateCreatedDoesNotMatchAnyExistingDateCreated_ReturnsBadRequest()
        {
            //Arrange
            Guid bookingId = Guid.NewGuid();
            Guid jobID = Guid.NewGuid();
            string consumerID = "850132958321456545893";

            Booking bookingDto = new Booking
            {
                BookingId = bookingId,
                JobId = jobID,
                ConsumerId = consumerID,
                Address = "Rachelsmolen 1",
                City = "Eindhoven",
                BookingDate = DateTime.Now
            };

            Mock<IJobDbHandler> jMock = new Mock<IJobDbHandler>();
            jMock.Setup(k => k.GetJobAsync(bookingDto.JobId)).ReturnsAsync(new Job());

            Mock<IUserDbHandler> uMock = new Mock<IUserDbHandler>();
            uMock.Setup(k => k.GetUserAsync(bookingDto.ConsumerId)).ReturnsAsync(new User());

            this._bookingMock.Setup(p => p.GetBookingAsync(bookingDto.BookingId))
                .ReturnsAsync(new Booking
                {
                    BookingId = bookingDto.BookingId,
                    JobId = bookingDto.JobId,
                    ConsumerId = bookingDto.ConsumerId,
                    DateCreated = new DateTime(2001, 7, 2)
                });

            this._controller = new BookingsController(this._bookingMock.Object, jMock.Object, uMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = consumerID });

            //Act
            var response = await this._controller.UpdateBooking(bookingDto.BookingId, bookingDto);

            //Assert

            Assert.IsType<BadRequestObjectResult>(response);
        }

        #endregion

        #region Delete

        [Fact]
        public async Task DeleteBooking_ReturnsOkResult()
        {
            // Arrange
            Guid bookingId = Guid.NewGuid();
            string userId = "110811874816751097523";

            this._bookingMock.Setup(p => p.GetBookingAsync(bookingId)).ReturnsAsync(new Booking { BookingId = bookingId, ConsumerId = userId });
            this._controller = new BookingsController(this._bookingMock.Object, new Mock<IJobDbHandler>().Object, new Mock<IUserDbHandler>().Object);

            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = userId });

            // Act
            ActionResult okResponse = await this._controller.DeleteBooking(bookingId);

            // Assert
            Assert.IsType<OkResult>(okResponse);
        }

        [Fact]
        public async Task DeleteBooking_ReturnsNotFound()
        {
            // Arrange
            Guid bookingId = new Guid("debe96be-0000-0000-0000-aba156b5ef74");
            string userId = "110811874816751097523";

            this._bookingMock.Setup(p => p.GetBookingAsync(bookingId)).ThrowsAsync(new ArgumentException());
            this._controller = new BookingsController(this._bookingMock.Object, new Mock<IJobDbHandler>().Object, new Mock<IUserDbHandler>().Object);

            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = userId });

            // Act
            var response = await this._controller.DeleteBooking(bookingId);

            // Assert
            Assert.IsType<NotFoundObjectResult>(response);
        }

        [Fact]
        public async Task DeleteBooking_WhenWrongHttpContext_ReturnsUnauthorizedResult()
        {
            // Arrange
            Guid bookingId = Guid.NewGuid();
            string userId = "110811874816751097523";

            this._bookingMock.Setup(p => p.GetBookingAsync(bookingId)).ReturnsAsync(new Booking { BookingId = bookingId, ConsumerId = userId });
            this._controller = new BookingsController(this._bookingMock.Object, new Mock<IJobDbHandler>().Object, new Mock<IUserDbHandler>().Object);


            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = "110811874816751090000" });

            // Act
            ActionResult okResponse = await this._controller.DeleteBooking(bookingId);

            // Assert
            Assert.IsType<UnauthorizedResult>(okResponse);
        }
#endregion

        [Fact]
        public async Task CreateBooking_MustHaveAuthorizeAttribute()
        {
            // Arrange
            MethodBase method = typeof(BookingsController).GetMethod(nameof(this._controller.CreateBooking));

            // Act & Assert
            object[] attributes = method.GetCustomAttributes(typeof(AuthorizeAttribute), true);

            // Assert
            Assert.Single(attributes);
        }

        [Fact]
        public async Task UpdateBooking_MustHaveAuthorizeAttribute()
        {
            // Arrange
            MethodBase method = typeof(BookingsController).GetMethod(nameof(this._controller.CreateBooking));

            // Act & Assert
            object[] attributes = method.GetCustomAttributes(typeof(AuthorizeAttribute), true);

            // Assert
            Assert.Single(attributes);
        }

        [Fact]
        public async Task DeleteBooking_MustHaveAuthorizeAttribute()
        {
            // Arrange
            MethodBase method = typeof(BookingsController).GetMethod(nameof(this._controller.CreateBooking));

            // Act & Assert
            object[] attributes = method.GetCustomAttributes(typeof(AuthorizeAttribute), true);

            // Assert
            Assert.Single(attributes);
        }
    }
}
