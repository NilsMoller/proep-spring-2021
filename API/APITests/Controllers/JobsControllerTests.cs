using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

using API.Attributes;
using API.Controllers;
using API.Helpers;
using Core.Db;
using Core.Models;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Moq;

using Xunit;

namespace APITests.Controllers
{
    public class JobsControllerTests : IDisposable
    {
        public Mock<IJobDbHandler> _jobMock;
        public Mock<IUserDbHandler> _userDbHandler;
        JobsController _controller;

        // setup
        public JobsControllerTests()
        {
            _jobMock = new Mock<IJobDbHandler>();
            _userDbHandler = new Mock<IUserDbHandler>();
        }

        // breakdown
        public void Dispose()
        {

        }

        [Fact]
        public async Task GetJobs_ReturnsOK()
        {
            this._jobMock.Setup(p => p.GetJobsAsync()).ReturnsAsync(new List<Job>());
            this._controller = new JobsController(this._jobMock.Object, new Mock<IUserDbHandler>().Object);


            //Act
            ActionResult<List<Job>> response = await this._controller.GetJobs();

            //Assert
            OkObjectResult result = response.Result as OkObjectResult;

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task GetJobAsync_ReturnsJob_WhenMatchingIDExists()
        {
            //Arrange
            Guid jobID = Guid.NewGuid();
            string jobTitle = "Photographer";

            Job jobDto = new Job
            {
                JobId = jobID,
                Title = jobTitle,
                Price = 27.12
            };
            this._jobMock.Setup(p => p.GetJobAsync(jobID)).ReturnsAsync(jobDto);
            this._controller = new JobsController(this._jobMock.Object, new Mock<IUserDbHandler>().Object);

            //Act
            ActionResult<Job> response = await this._controller.GetJob(jobID);

            //Assert
            OkObjectResult result = response.Result as OkObjectResult;
            Job jobResponse = result.Value as Job;

            Assert.Equal(jobDto, jobResponse);
        }

        [Fact]
        public async Task DeleteJob_ReturnsOkResult()
        {
            // Arrange
            Guid jobID = new Guid("dba2cb60-c1db-4317-86e0-fe1b17c73b0a");
            string userId = "110811874816751097523";

            this._jobMock.Setup(p => p.GetJobAsync(jobID)).ReturnsAsync(new Job { JobId = jobID, ProviderId = userId });
            this._controller = new JobsController(this._jobMock.Object, new Mock<IUserDbHandler>().Object);

            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = userId });

            // Act
            ActionResult okResponse = await this._controller.DeleteJob(jobID);

            // Assert
            Assert.IsType<OkResult>(okResponse);
        }

        [Fact]
        public async Task DeleteJob_WhenWrongHttpContext_ReturnsUnauthorizedResult()
        {
            // Arrange
            Guid jobID = new Guid("dba2cb60-c1db-4317-86e0-fe1b17c73b0a");
            string userId = "110811874816751097523";

            this._jobMock.Setup(p => p.GetJobAsync(jobID)).ReturnsAsync(new Job { JobId = jobID, ProviderId = userId });
            this._controller = new JobsController(this._jobMock.Object, new Mock<IUserDbHandler>().Object);

            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = "110811874816751097000" });

            // Act
            ActionResult okResponse = await this._controller.DeleteJob(jobID);

            // Assert
            Assert.IsType<UnauthorizedResult>(okResponse);
        }


        [Fact]
        public async Task DeleteJob_WhenIdDoesNotExist_ReturnsNotFound()
        {
            // Arrange
            Guid jobID = new Guid("dba2cb60-c1db-4317-86e0-fe1b17c73b0a");



            this._jobMock.Setup(p => p.GetJobAsync(jobID)).ThrowsAsync(new ArgumentException());
            this._controller = new JobsController(this._jobMock.Object, new Mock<IUserDbHandler>().Object);

            // Act
            ActionResult okResponse = await this._controller.DeleteJob(jobID);

            // Assert
            Assert.IsType<NotFoundObjectResult>(okResponse);
        }

        [Fact]
        public async Task CreateJob_MustHaveAuthorizeAttribute()
        {
            // Arrange
            MethodBase method = typeof(JobsController).GetMethod(nameof(this._controller.CreateJob));

            // Act & Assert
            object[] attributes = method.GetCustomAttributes(typeof(AuthorizeAttribute), true);

            // Assert
            Assert.Single(attributes);
        }

        [Fact]
        public async Task UpdateJob_MustHaveAuthorizeAttribute()
        {
            // Arrange
            MethodBase method = typeof(JobsController).GetMethod(nameof(this._controller.UpdateJob));

            // Act & Assert
            object[] attributes = method.GetCustomAttributes(typeof(AuthorizeAttribute), true);

            // Assert
            Assert.Single(attributes);
        }

        [Fact]
        public async Task DeleteJob_MustHaveAuthorizeAttribute()
        {
            // Arrange
            MethodBase method = typeof(JobsController).GetMethod(nameof(this._controller.DeleteJob));

            // Act & Assert
            object[] attributes = method.GetCustomAttributes(typeof(AuthorizeAttribute), true);

            // Assert
            Assert.Single(attributes);
        }

        [Fact]
        public async Task GetJobsForUser_ReturnsOK()
        {
            string _userID = GoogleUserIdGenerator.GenerateRandomGoogleUserId();
            this._jobMock.Setup(p => p.GetJobsAsync()).ReturnsAsync(new List<Job>());
            this._controller = new JobsController(this._jobMock.Object, new Mock<IUserDbHandler>().Object);


            //Act
            ActionResult<List<Job>> response = await this._controller.GetJobsForUser(_userID);

            //Assert
            OkObjectResult result = response.Result as OkObjectResult;

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task GetJobsForUser_ReturnsBadRequest()
        {
            string _userID = GoogleUserIdGenerator.GenerateRandomGoogleUserId();
            //this._jobMock.Setup(p => p.GetJobsAsync()).ReturnsAsync(new List<Job>());
            this._userDbHandler.Setup(p => p.GetUserAsync(_userID)).ThrowsAsync(new ArgumentException());
            this._controller = new JobsController(this._jobMock.Object, new Mock<IUserDbHandler>().Object);


            //Act
            ActionResult<List<Job>> response = await this._controller.GetJobsForUser(_userID);

            //Assert
            BadRequestObjectResult result = response.Result as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
