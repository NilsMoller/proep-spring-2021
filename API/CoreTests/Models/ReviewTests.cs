﻿using System;
using System.Threading.Tasks;

using Xunit;

namespace Core.Models.Tests
{
    public class ReviewTests : IDisposable
    {
        private readonly Review review;

        // setup
        public ReviewTests()
        {
            this.review = new Review { ReviewId = Guid.Empty };
        }

        // breakdown
        public void Dispose()
        {

        }

        [Fact]
        public async Task Review_EqualsObject_ReturnsTrueWhenIdsMatch()
        {
            // Arrange
            object reviewCheck = new Review { ReviewId = Guid.Empty };

            // Act
            bool result = this.review.Equals(reviewCheck);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task Review_EqualsObject_ReturnsFalseWhenIdsDoNotMatch()
        {
            // Arrange
            object reviewCheck = new Review { ReviewId = Guid.NewGuid() };

            // Act
            bool result = this.review.Equals(reviewCheck);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task Review_EqualsReview_ReturnsTrueWhenIdsMatch()
        {
            // Arrange
            Review reviewCheck = new Review { ReviewId = Guid.Empty };

            // Act
            bool result = this.review.Equals(reviewCheck);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task Review_EqualsReview_ReturnsFalseWhenIdsDoNotMatch()
        {
            // Arrange
            Review reviewCheck = new Review { ReviewId = Guid.NewGuid() };

            // Act
            bool result = this.review.Equals(reviewCheck);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task Review_EqualsOperator_ReturnsTrueWhenIdsMatch()
        {
            // Arrange
            Review reviewCheck = new Review { ReviewId = Guid.Empty };

            // Act
            bool result = this.review == reviewCheck;

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task Review_EqualsOperator_ReturnsFalseWhenIdsDoNotMatch()
        {
            // Arrange
            Review reviewCheck = new Review { ReviewId = Guid.NewGuid() };

            // Act
            bool result = this.review == reviewCheck;

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task Review_NotEqualsOperator_ReturnsTrueWhenIdsDoNotMatch()
        {
            // Arrange
            Review reviewCheck = new Review { ReviewId = Guid.NewGuid() };

            // Act
            bool result = this.review != reviewCheck;

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task Review_NotEqualsOperator_ReturnsFalseWhenIdsMatch()
        {
            // Arrange
            Review reviewCheck = new Review { ReviewId = Guid.Empty };

            // Act
            bool result = this.review != reviewCheck;

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task Review_GetHashCode_IsEqualWhenIdIsEqual()
        {
            // Arrange
            Review reviewCheck = new Review { ReviewId = Guid.Empty };

            // Act
            int reviewCode = this.review.GetHashCode();
            int reviewCheckCode = reviewCheck.GetHashCode();

            // Assert
            Assert.Equal(reviewCheckCode, reviewCode);
        }

        [Fact]
        public async Task Review_GetHashCode_IsDifferentWhenIdIsDifferent()
        {
            // Arrange
            Review reviewCheck = new Review { ReviewId = Guid.NewGuid() };

            // Act
            int reviewCode = this.review.GetHashCode();
            int reviewCheckCode = reviewCheck.GetHashCode();

            // Assert
            Assert.NotEqual(reviewCheckCode, reviewCode);
        }
    }
}