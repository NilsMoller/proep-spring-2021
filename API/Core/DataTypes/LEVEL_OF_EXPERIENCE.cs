﻿namespace Core.DataTypes
{
    /// <summary>
    /// The level of experience the person has for the job.
    /// </summary>
    public enum LEVEL_OF_EXPERIENCE
    {
        /// <summary>
        /// Here for defaulting purposes. <b>Do not use.</b>
        /// </summary>
        Empty = 0,

        /// <summary>
        /// Beginner level. 0-1 years of experience.
        /// </summary>
        Beginner = 1,

        /// <summary>
        /// Intermediary level. 1-3 years of experience.
        /// </summary>
        Intermediate = 2,

        /// <summary>
        /// Advanced level. 3-5 years of experience.
        /// </summary>
        Advanced = 3,

        /// <summary>
        /// Expert level. 5+ years of experience.
        /// </summary>
        Expert = 4
    }
}
