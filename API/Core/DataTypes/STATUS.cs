﻿namespace Core.DataTypes
{
    /// <summary>
    /// The current status of the booking.
    /// </summary>
    public enum STATUS
    {
        /// <summary>
        /// The booking is pending.
        /// </summary>
        Pending = 0,

        /// <summary>
        /// The booking has been accepted.
        /// </summary>
        Accepted = 1,

        /// <summary>
        /// The booking is in progress.
        /// </summary>
        InProgress = 2,

        /// <summary>
        /// The booking has been completed.
        /// </summary>
        Completed = 3,

        /// <summary>
        /// The booking has been denied by the job provider.
        /// </summary>
        Denied = 4
    }
}
