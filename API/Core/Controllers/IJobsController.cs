﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Core.Models;

using Microsoft.AspNetCore.Mvc;

namespace Core.Controllers
{
    /// <summary>
    /// This interface defines structure of the interaction with jobs.
    /// </summary>
    public interface IJobsController
    {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        Task<ActionResult<List<Job>>> GetJobs();
        Task<ActionResult<Job>> GetJob(Guid id);
        Task<ActionResult<List<Job>>> GetJobsForUser(string userId);
        Task<ActionResult<Job>> CreateJob(Job job);
        Task<ActionResult> UpdateJob(Guid id, Job job);
        Task<ActionResult> DeleteJob(Guid id);
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    }
}
