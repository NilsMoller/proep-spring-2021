﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Core.Models;

using Microsoft.AspNetCore.Mvc;

namespace Core.Controllers
{
    /// <summary>
    /// This interface defines the structure of a bookings controller.
    /// </summary>
    public interface IBookingsController
    {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        Task<ActionResult<List<Booking>>> GetBookings();
        Task<ActionResult<Booking>> GetBooking(Guid id);
        Task<ActionResult<List<Booking>>> GetBookingsForJob(Guid jobId);
        Task<ActionResult<Booking>> CreateBooking(Booking booking);
        Task<ActionResult> UpdateBooking(Guid id, Booking booking);
        Task<ActionResult> DeleteBooking(Guid id);
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    }
}
