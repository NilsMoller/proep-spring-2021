﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

using Core.DataTypes;

namespace Core.Models
{
    /// <summary>
    /// This class describes how a booking looks. Every user can have many of these. <br />
    /// See this as hiring someone to do a job they provide.
    /// </summary>
    public class Booking : IEquatable<Booking>
    {
        /// <summary>
        /// The id of this booking.
        /// </summary>
        [Key]
        [ExcludeFromCodeCoverage]
        public Guid BookingId { get; set; }

        /// <summary>
        /// The id of the job this booking belongs to.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public Guid JobId { get; set; }

        /// <summary>
        /// The id of the user booking the related job.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public string ConsumerId { get; set; }

        /// <summary>
        /// The address this booking is from.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public string Address { get; set; }

        /// <summary>
        /// The city this booking is from.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public string City { get; set; }

        /// <summary>
        /// The current status of the booking.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public STATUS Status { get; set; }

        /// <summary>
        /// The date at which this booking will be executed.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public DateTime BookingDate { get; set; }

        /// <summary>
        /// The date this booking was created.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// The date this booking was updated last.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public DateTime? LastUpdated { get; set; }

        /// <summary>
        /// Checks whether an object is equal to this object.
        /// </summary>
        /// <param name="obj">The object to compare this object to.</param>
        /// <returns>True if the ids match. False otherwise.</returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as Booking);
        }

        /// <summary>
        /// Checks whether an object is equal to this object.
        /// </summary>
        /// <param name="other">The object to compare this object to.</param>
        /// <returns>True if the ids match. False otherwise.</returns>
        public bool Equals(Booking other)
        {
            return other != null &&
                   this.BookingId.Equals(other.BookingId);
        }

        /// <summary>
        /// Generates the hash code for this object using the id.
        /// </summary>
        /// <returns>The generated hash code.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(this.BookingId);
        }

        /// <summary>
        /// Checks whether left is equal to right based on id.
        /// </summary>
        /// <param name="left">The left side of the operator to evaluate.</param>
        /// <param name="right">The right side of the operator to evaluate.</param>
        /// <returns>True if ids are equal. False otherwise.</returns>
        public static bool operator ==(Booking left, Booking right)
        {
            return EqualityComparer<Booking>.Default.Equals(left, right);
        }

        /// <summary>
        /// Checks whether left is not equal to right based on id.
        /// </summary>
        /// <param name="left">The left side of the operator to evaluate.</param>
        /// <param name="right">The right side of the operator to evaluate.</param>
        /// <returns>True if ids are not equal. False otherwise.</returns>
        public static bool operator !=(Booking left, Booking right)
        {
            return !(left == right);
        }
    }
}
