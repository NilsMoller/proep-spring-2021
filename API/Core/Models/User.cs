﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Core.Models
{
    /// <summary>
    /// This class describes a particular user. <br></br>
    /// This user can either be a job poster or job seeker.
    /// </summary>
    public class User : IEquatable<User>
    {
        /// <summary>
        /// The id of the user.
        /// </summary>
        [Key]
        [ExcludeFromCodeCoverage]
        public string UserId { get; set; }

        /// <summary>
        /// The email address of this user.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public string Email { get; set; }

        /// <summary>
        /// The first name of this user.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public string FirstName { get; set; }

        /// <summary>
        /// The last name of this user.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public string LastName { get; set; }

        /// <summary>
        /// The description of a user.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public string Description { get; set; }

        /// <summary>
        /// The date of birth of the user.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// The profile picture url link of this user.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public string ProfilePictureLink { get; set; }

        /// <summary>
        /// The phone number of this user.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public string Phone { get; set; }

        /// <summary>
        /// The address of this user.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public string Address { get; set; }

        /// <summary>
        /// The user's city.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public string City { get; set; }

        /// <summary>
        /// The current rating of this user.
        /// This is a single decimal point number between 0 and 5.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public float Rating { get; set; }

        /// <summary>
        /// The date this user profile was created.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// The date this user logged in last.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public DateTime LastLogin { get; set; }

        /// <summary>
        /// Checks whether an object is equal to this object.
        /// </summary>
        /// <param name="obj">The object to compare this object to.</param>
        /// <returns>True if the ids match. False otherwise.</returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as User);
        }

        /// <summary>
        /// Checks whether an object is equal to this object.
        /// </summary>
        /// <param name="other">The object to compare this object to.</param>
        /// <returns>True if the ids match. False otherwise.</returns>
        public bool Equals(User other)
        {
            return other != null &&
                   this.UserId == other.UserId;
        }

        /// <summary>
        /// Generates the hash code for this object using the id.
        /// </summary>
        /// <returns>The generated hash code.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(this.UserId);
        }

        /// <summary>
        /// Checks whether left is equal to right based on id.
        /// </summary>
        /// <param name="left">The left side of the operator to evaluate.</param>
        /// <param name="right">The right side of the operator to evaluate.</param>
        /// <returns>True if ids are equal. False otherwise.</returns>
        public static bool operator ==(User left, User right)
        {
            return EqualityComparer<User>.Default.Equals(left, right);
        }

        /// <summary>
        /// Checks whether left is not equal to right based on id.
        /// </summary>
        /// <param name="left">The left side of the operator to evaluate.</param>
        /// <param name="right">The right side of the operator to evaluate.</param>
        /// <returns>True if ids are not equal. False otherwise.</returns>
        public static bool operator !=(User left, User right)
        {
            return !(left == right);
        }
    }
}
