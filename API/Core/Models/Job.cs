﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

using Core.DataTypes;

namespace Core.Models
{
    /// <summary>
    /// This class describes how a job looks. Every user can have many of these. <br></br>
    /// See this as a "Plumbing" job. Service consumers can book this job multiple times. A user can only provide one service of each type.
    /// </summary>
    public class Job : IEquatable<Job>
    {
        /// <summary>
        /// The id of this job.
        /// </summary>
        [Key]
        [ExcludeFromCodeCoverage]
        public Guid JobId { get; set; }

        /// <summary>
        /// The id of the user providing this job.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public string ProviderId { get; set; }

        /// <summary>
        /// The price of this job.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public double Price { get; set; }

        /// <summary>
        /// The title of this job.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public string Title { get; set; }

        /// <summary>
        /// The description of this job.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public string Description { get; set; }

        /// <summary>
        /// The level of experience the user executing this job has.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public LEVEL_OF_EXPERIENCE LevelOfExperience { get; set; }

        /// <summary>
        /// The date this job was created.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// The date this job was updated last.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public DateTime? LastUpdated { get; set; }

        /// <summary>
        /// Checks whether an object is equal to this object.
        /// </summary>
        /// <param name="obj">The object to compare this object to.</param>
        /// <returns>True if the ids match. False otherwise.</returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as Job);
        }

        /// <summary>
        /// Checks whether an object is equal to this object.
        /// </summary>
        /// <param name="other">The object to compare this object to.</param>
        /// <returns>True if the ids match. False otherwise.</returns>
        public bool Equals(Job other)
        {
            return other != null &&
                   this.JobId.Equals(other.JobId);
        }

        /// <summary>
        /// Generates the hash code for this object using the id.
        /// </summary>
        /// <returns>The generated hash code.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(this.JobId);
        }

        /// <summary>
        /// Checks whether left is equal to right based on id.
        /// </summary>
        /// <param name="left">The left side of the operator to evaluate.</param>
        /// <param name="right">The right side of the operator to evaluate.</param>
        /// <returns>True if ids are equal. False otherwise.</returns>
        public static bool operator ==(Job left, Job right)
        {
            return EqualityComparer<Job>.Default.Equals(left, right);
        }

        /// <summary>
        /// Checks whether left is not equal to right based on id.
        /// </summary>
        /// <param name="left">The left side of the operator to evaluate.</param>
        /// <param name="right">The right side of the operator to evaluate.</param>
        /// <returns>True if ids are not equal. False otherwise.</returns>
        public static bool operator !=(Job left, Job right)
        {
            return !(left == right);
        }
    }
}
