﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

using Core.Wrappers;

using Google.Apis.Auth;
using Google.Apis.Util;

namespace API.Wrappers
{
    /// <summary>
    /// This class implements the wrapper for the static Google.Apis.Auth.GoogleJsonWebSignature.ValidateAsync(string token)
    /// </summary>
    public class ValidateAsyncWrapper : IValidateAsyncWrapper
    {
        /// <inheritdoc/>
        [ExcludeFromCodeCoverage]
        public Task<GoogleJsonWebSignature.Payload> ValidateAsync(string jwt, IClock clock = null, bool forceGoogleCertRefresh = false)
        {
            return GoogleJsonWebSignature.ValidateAsync(jwt, clock, forceGoogleCertRefresh);
        }
    }
}
