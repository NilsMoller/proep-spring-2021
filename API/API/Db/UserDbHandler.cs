﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Core.Db;
using Core.Models;

using Microsoft.EntityFrameworkCore;

namespace API.Db
{
    /// <summary>
    /// This class handles database interaction for users.
    /// </summary>
    public class UserDbHandler : IUserDbHandler
    {
        private readonly DbContext context;

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="context"></param>
        public UserDbHandler(DbContext context)
        {
            this.context = context;
        }

        #region Async
        /// <inheritdoc/>
        public async Task<List<User>> GetUsersAsync()
        {
            return await this.context.Users
                .ToListAsync();
        }

        /// <inheritdoc/>
        public async Task<User> GetUserAsync(string id)
        {
            User user = await this.context.Users.FirstOrDefaultAsync(user => user.UserId == id);
            if (user == null)
                throw new ArgumentException("No user found with id " + id);

            return await this.context.Users
                .FirstAsync(user => user.UserId == id);
        }

        /// <inheritdoc/>
        public async Task CreateUserAsync(User user, bool saveChanges = true)
        {
            this.context.Users.Add(user);
            if (saveChanges)
                await this.context.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task UpdateUserAsync(User user, bool saveChanges = true)
        {
            try
            {
                if (!await this.context.Users.AnyAsync(userCheck => user.UserId == userCheck.UserId))
                    throw new ArgumentException("No user found with id " + user.UserId);

                this.context.Entry(user).State = EntityState.Modified;
                if (saveChanges)
                    await this.context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        /// <inheritdoc/>
        public async Task DeleteUserAsync(string id, bool saveChanges = true)
        {
            User user = await this.context.Users.FindAsync(id);

            if (user == null)
                throw new ArgumentException("No user found with id " + id);

            this.context.Users.Remove(user);
            if (saveChanges)
                await this.context.SaveChangesAsync();
        }
        #endregion

        #region Sync
        /// <inheritdoc/>
        public List<User> GetUsers()
        {
            return this.context.Users
                .ToList();
        }

        /// <inheritdoc/>
        public User GetUser(string id)
        {
            User user = this.context.Users
                .First(user => user.UserId == id);

            if (user == null)
                throw new ArgumentException("No user found with id " + id);

            return user;
        }

        /// <inheritdoc/>
        public void CreateUser(User user, bool saveChanges = true)
        {
            this.context.Users.Add(user);
            if (saveChanges)
                this.context.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateUser(User user, bool saveChanges = true)
        {
            try
            {
                if (!this.context.Users.Any(userCheck => user.UserId == userCheck.UserId))
                    throw new ArgumentException("No user found with id " + user.UserId);

                this.context.Entry(user).State = EntityState.Modified;
                if (saveChanges)
                    this.context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        /// <inheritdoc/>
        public void DeleteUser(string id, bool saveChanges = true)
        {
            User user = this.context.Users.Find(id);
            if (user == null)
                throw new ArgumentException("No user found with id " + id);

            this.context.Users.Remove(user);
            if (saveChanges)
                this.context.SaveChanges();
        }
        #endregion
    }
}
