﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Core.Db;
using Core.Models;

using Microsoft.EntityFrameworkCore;

namespace API.Db
{
    /// <summary>
    /// This class handles database interaction for reviews.
    /// </summary>
    public class ReviewDbHandler : IReviewDbHandler
    {
        private readonly DbContext context;

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="context"></param>
        public ReviewDbHandler(DbContext context)
        {
            this.context = context;
        }

        #region Async
        /// <inheritdoc/>
        public async Task<List<Review>> GetReviewsAsync()
        {
            List<Review> reviews = await this.context.Reviews
                .ToListAsync();

            return reviews;
        }

        /// <inheritdoc/>
        public async Task<Review> GetReviewAsync(Guid reviewId)
        {
            Review review = await this.context.Reviews
                .FirstOrDefaultAsync(review => review.ReviewId == reviewId);
            if (review == null)
                throw new ArgumentException("No review found with id " + reviewId);

            return review;
        }

        /// <inheritdoc/>
        public async Task CreateReviewAsync(Review review, bool saveChanges = true)
        {
            this.context.Reviews.Add(review);
            if (saveChanges)
                await this.context.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task DeleteReviewAsync(Guid reviewId, bool saveChanges = true)
        {
            Review review = await this.context.Reviews.FirstOrDefaultAsync(review => review.ReviewId == reviewId);
            if (review == null)
                throw new ArgumentException("No review found with id " + reviewId);

            this.context.Reviews.Remove(review);
            if (saveChanges)
                await this.context.SaveChangesAsync();
        }
        #endregion

        #region Sync
        /// <inheritdoc/>
        public List<Review> GetReviews()
        {
            List<Review> reviews = this.context.Reviews
                .ToList();

            return reviews;
        }

        /// <inheritdoc/>
        public Review GetReview(Guid reviewId)
        {
            Review review = this.context.Reviews
                .FirstOrDefault(review => review.ReviewId == reviewId);

            if (review == null)
                throw new ArgumentException("No review found with id " + reviewId);

            return review;
        }

        /// <inheritdoc/>
        public void CreateReview(Review review, bool saveChanges = true)
        {
            this.context.Reviews.Add(review);

            if (saveChanges)
                this.context.SaveChanges();
        }

        /// <inheritdoc/>
        public void DeleteReview(Guid reviewId, bool saveChanges = true)
        {
            Review review = this.context.Reviews.Find(reviewId);
            if (review == null)
                throw new ArgumentException("No review found with reviewer id " + reviewId);

            this.context.Reviews.Remove(review);

            if (saveChanges)
                this.context.SaveChanges();
        }
        #endregion
    }
}
