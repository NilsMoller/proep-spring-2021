﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Core.Db;
using Core.Models;

using Microsoft.EntityFrameworkCore;

namespace API.Db
{
    /// <summary>
    /// This class handles database interaction for jobs.
    /// </summary>
    public class JobDbHandler : IJobDbHandler
    {
        private readonly DbContext context;

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="context"></param>
        public JobDbHandler(DbContext context)
        {
            this.context = context;
        }

        #region Async
        /// <inheritdoc/>
        public async Task<List<Job>> GetJobsAsync()
        {
            return await this.context.Jobs
                .ToListAsync();
        }

        /// <inheritdoc/>
        public async Task<Job> GetJobAsync(Guid id)
        {
            if (!await this.context.Jobs.AnyAsync(job => job.JobId == id))
                throw new ArgumentException("No job found with id " + id);

            Job job = await this.context.Jobs.FirstAsync(job => job.JobId == id);

            return job;
        }

        /// <inheritdoc/>
        public async Task CreateJobAsync(Job job, bool saveChanges = true)
        {
            this.context.Jobs.Add(job);

            if (saveChanges)
                await this.context.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task UpdateJobAsync(Job job, bool saveChanges = true)
        {
            try
            {
                if (!await this.context.Jobs.AnyAsync(jobCheck => job.JobId == jobCheck.JobId))
                    throw new ArgumentException("No job found with id " + job.JobId);

                this.context.Entry(job).State = EntityState.Modified;
                if (saveChanges)
                    await this.context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        /// <inheritdoc/>
        public async Task DeleteJobAsync(Guid id, bool saveChanges = true)
        {
            if (!await this.context.Jobs.AnyAsync(job => job.JobId == id))
                throw new ArgumentException("No job found with id " + id);

            Job job = await this.context.Jobs.FindAsync(id);
            this.context.Jobs.Remove(job);

            if (saveChanges)
                await this.context.SaveChangesAsync();
        }
        #endregion

        #region Sync
        /// <inheritdoc/>
        public List<Job> GetJobs()
        {
            List<Job> jobs = this.context.Jobs
                .ToList();

            return jobs;
        }

        /// <inheritdoc/>
        public Job GetJob(Guid id)
        {
            Job job = this.context.Jobs
                .FirstOrDefault(job => job.JobId == id);

            if (job == null)
                throw new ArgumentException("No job found with id " + id);

            return job;
        }

        /// <inheritdoc/>
        public void CreateJob(Job job, bool saveChanges = true)
        {
            this.context.Jobs.Add(job);
            if (saveChanges)
                this.context.SaveChanges();
        }

        /// <inheritdoc/>
        public void UpdateJob(Job job, bool saveChanges = true)
        {
            try
            {
                Job jobCheck = this.context.Jobs.Find(job.JobId);
                if (jobCheck == null)
                    throw new ArgumentException("No job found with id " + job.JobId);

                this.context.Entry(job).State = EntityState.Modified;

                if (saveChanges)
                    this.context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        /// <inheritdoc/>
        public void DeleteJob(Guid id, bool saveChanges = true)
        {
            Job job = this.context.Jobs.Find(id);
            if (job == null)
                throw new ArgumentException("No job found with id " + id);

            this.context.Jobs.Remove(job);
            if (saveChanges)
                this.context.SaveChanges();
        }
        #endregion
    }
}
