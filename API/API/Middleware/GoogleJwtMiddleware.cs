﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Core.Db;
using Core.Models;
using Core.Wrappers;

using Microsoft.AspNetCore.Http;

using static Google.Apis.Auth.GoogleJsonWebSignature;

namespace API.Middleware
{
    /// <summary>
    /// This middleware authenticates the token in the authorization header with google.
    /// </summary>
    public class GoogleJwtMiddleware
    {
        private readonly RequestDelegate next;

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="next">The next request in the pipeline.</param>
        public GoogleJwtMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        /// <inheritdoc />
        public async Task InvokeAsync(HttpContext context, IUserDbHandler userDbHandler, IValidateAsyncWrapper validateAsyncWrapper)
        {
            string token = context.Request.Headers["Authorization"].FirstOrDefault();
            if (token != null)
            {
                try
                {
                    Payload googleUser = await validateAsyncWrapper.ValidateAsync(token);

                    User user = await userDbHandler.GetUserAsync(googleUser.Subject);
                    user.LastLogin = DateTime.Now;
                    await userDbHandler.UpdateUserAsync(user);

                    if (context.Items.ContainsKey("User"))
                        context.Items.Remove("User");
                    context.Items.Add("User", user);
                }
                catch
                {
                    // do nothing if jwt validation fails
                    // user is not attached to context so request won't have access to secure routes
                }
            }

            await this.next(context);
        }
    }
}
