﻿using System;

namespace API.Helpers
{
    /// <summary>
    /// This class holds the functionality to generate a random, non-existant google user id.<br></br>
    /// Useful for mocking.
    /// </summary>
    public static class GoogleUserIdGenerator
    {
        /// <summary>
        /// Generates a random google user id. <br></br>
        /// The id is 21 characters long, where every character is a whole number between 0 and 9 (inclusive).
        /// </summary>
        /// <returns>The randomly generated id.</returns>
        public static string GenerateRandomGoogleUserId()
        {
            Random rng = new Random();
            string generatedId = "";

            for (int i = 0; i < 21; i++) // a google user id has 21 numbers
            {
                generatedId += rng.Next(10).ToString();
            }

            return generatedId;
        }
    }
}
