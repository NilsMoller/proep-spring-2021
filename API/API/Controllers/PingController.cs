﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    /// <summary>
    /// This controller provides ping functionality to make sure a client can connect to the API.
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class PingController : ControllerBase
    {
        /// <summary>
        /// Ping the API to test connection.
        /// </summary>
        /// <returns>Pong</returns>
        [HttpGet]
        public Task<string> Ping()
        {
            return Task.FromResult("Pong");
        }
    }
}
