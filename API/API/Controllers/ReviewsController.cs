﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using API.Attributes;

using Core.Controllers;
using Core.Db;
using Core.Models;

using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    /// <summary>
    /// This controller handles the interaction with reviews.
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class ReviewsController : Controller, IReviewsController
    {
        private IReviewDbHandler ReviewDbHandler { get; }
        private IUserDbHandler UserDbHandler { get; }
        private IBookingDbHandler BookingDbHandler { get; }
        private IJobDbHandler JobDbHandler { get; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="reviewDbHandler">The database context for reviews.</param>
        /// <param name="userDbHandler">The database context for users.</param>
        /// <param name="bookingDbHandler">The database context for bookings.</param>
        /// <param name="jobDbHandler">The database context for jobs.</param>
        public ReviewsController(IReviewDbHandler reviewDbHandler, IUserDbHandler userDbHandler, IBookingDbHandler bookingDbHandler, IJobDbHandler jobDbHandler)
        {
            this.ReviewDbHandler = reviewDbHandler;
            this.UserDbHandler = userDbHandler;
            this.BookingDbHandler = bookingDbHandler;
            this.JobDbHandler = jobDbHandler;
        }

        // GET: Reviews
        /// <summary>
        /// Gets all the reviews.
        /// </summary>
        /// <returns>
        /// Ok() with the collection of all reviews. <br></br>
        /// </returns>
        [HttpGet]
        public async Task<ActionResult<List<Review>>> GetReviews()
        {
            return this.Ok(await this.ReviewDbHandler.GetReviewsAsync());
        }

        // GET: Reviews/00000000-0000-0000-0000-000000000000
        /// <summary>
        /// Get a review.
        /// </summary>
        /// <param name="reviewId">The id of the review to delete.</param>
        /// <returns>
        /// Ok() with the review with matching id. <br></br> 
        /// NotFound() if no review was found. <br></br>
        /// </returns>
        [HttpGet("reviewId/{reviewId}")]
        public async Task<ActionResult<Review>> GetReviewById(Guid reviewId)
        {
            try
            {
                return this.Ok(await this.ReviewDbHandler.GetReviewAsync(reviewId));
            }
            catch (ArgumentException e)
            {
                return this.NotFound(e.Message);
            }
        }

        // GET: Reviews/reviewerId/00000000-0000-0000-0000-000000000000
        /// <summary>
        /// Get reviews posted by a user.
        /// </summary>
        /// <param name="reviewerId">The id of the reviewer.</param>
        /// <returns>
        /// Ok() with the list of reviews. <br></br> 
        /// NotFound() if the reviewer does not exist. <br></br>
        /// </returns>
        [HttpGet("reviewerId/{reviewerId}")]
        public async Task<ActionResult<List<Review>>> GetReviewsByReviewer(string reviewerId) 
        {
            try
            {
                await this.UserDbHandler.GetUserAsync(reviewerId); // if user doesnt exist
                return this.Ok((await this.ReviewDbHandler.GetReviewsAsync()).Where(review => review.ReviewerId == reviewerId).ToList());
            }
            catch (ArgumentException e)
            {
                return this.NotFound(e.Message);
            }
        }

        // GET: Reviews/revieweeId/00000000-0000-0000-0000-000000000000
        /// <summary>
        /// Get reviews about a user.
        /// </summary>
        /// <param name="revieweeId">The id of the person being reviewed.</param>
        /// <returns>
        /// Ok() with the list of reviews. <br></br> 
        /// NotFound() if the reviewee does not exist. <br></br>
        /// </returns>
        [HttpGet("revieweeId/{revieweeId}")]
        public async Task<ActionResult<List<Review>>> GetReviewsByReviewee(string revieweeId)
        {
            try
            {
                await this.UserDbHandler.GetUserAsync(revieweeId); // if user doesnt exist

                return this.Ok((await this.ReviewDbHandler.GetReviewsAsync()).Where(review => review.RevieweeId == revieweeId).ToList());
            }
            catch (ArgumentException e)
            {
                return this.NotFound(e.Message);
            }
        }

        // GET: Reviews/00000000-0000-0000-0000-000000000000/00000000-0000-0000-0000-000000000000
        /// <summary>
        /// Get reviews about a user written by a user.
        /// </summary>
        /// <param name="reviewerId">The id of the user who posted the review.</param>
        /// <param name="revieweeId">The id of the uesr which the review was about.</param>
        /// <returns>
        /// Ok() with the list of reviews. <br></br> 
        /// NotFound() if the reviewer does not exist. <br></br>
        /// NotFound() if the reviewee does not exist. <br></br>
        /// </returns>
        [HttpGet("{reviewerId}/{revieweeId}")]
        public async Task<ActionResult<List<Review>>> GetReviewsByReviewerAndReviewee(string reviewerId, string revieweeId)
        {
            try
            {
                await this.UserDbHandler.GetUserAsync(reviewerId); // if user doesnt exist
                await this.UserDbHandler.GetUserAsync(revieweeId); // if user doesnt exist

                return this.Ok((await this.ReviewDbHandler.GetReviewsAsync()).Where(review => review.ReviewerId == reviewerId && review.RevieweeId == revieweeId).ToList());
            }
            catch (ArgumentException e)
            {
                return this.NotFound(e.Message);
            }
        }

        // POST: Reviews
        /// <summary>
        /// Creates a new review.
        /// </summary>
        /// <param name="review">The review to create.</param>
        /// <returns>
        /// Unauthorized() if the logged in user is not the reviewer. <br></br>
        /// BadRequest() if the rating is not between 0.1 and 5. <br></br>
        /// BadRequest() if the reviewer does not exist. <br></br> 
        /// BadRequest() if the reviewee does not exist. <br></br> 
        /// BadRequest() if the reviewer hasn't hired the reviewee. <br></br>
        /// BadRequest() if the reviewer already posted a review about the reviewee. <br></br>
        /// CreatedAtAction() with the URI to the newly created review and the review. <br></br>
        /// </returns>
        /// <remarks>
        /// Required fields:<br></br>
        /// ReviewerId<br></br>
        /// RevieweeId<br></br>
        /// Title<br></br>
        /// Description - optional<br></br>
        /// Rating<br></br>
        /// Pros - optional<br></br>
        /// Cons - optional<br></br>
        /// </remarks>
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<Review>> CreateReview(Review review)
        {
            try
            {
                if (((User)this.HttpContext.Items["User"]).UserId != review.ReviewerId)
                    return this.Unauthorized();

                if (review.Rating < 0.1 || review.Rating > 5)
                    return this.BadRequest("Rating must be between 0.1 and 5.");

                review.ReviewId = Guid.NewGuid();

                DateTime timeCreated = DateTime.Now;
                review.DateCreated = timeCreated;

                // make sure no excess reviews are being posted
                User reviewer = this.UserDbHandler.GetUser(review.ReviewerId);
                User reviewee = this.UserDbHandler.GetUser(review.RevieweeId);

                int amountOfReviewsAlreadyPostedAboutReviewee = (await this.ReviewDbHandler.GetReviewsAsync()).Where(reviewCheck => reviewCheck.ReviewerId == review.ReviewerId && reviewCheck.RevieweeId == review.RevieweeId).Count();
                if (amountOfReviewsAlreadyPostedAboutReviewee > 0)
                    return this.BadRequest("Reviewer already posted a review about reviewee.");

                await this.ReviewDbHandler.CreateReviewAsync(review);

                // update reviewee rating
                if (reviewee.Rating == -1)
                    reviewee.Rating = review.Rating;
                else
                {
                    int amountOfReviews = (await this.ReviewDbHandler.GetReviewsAsync()).Where(review => review.RevieweeId == review.RevieweeId).ToList().Count;
                    reviewee.Rating = Convert.ToSingle(Math.Round(((reviewee.Rating * (amountOfReviews - 1)) + review.Rating) / amountOfReviews, 1));

                    await this.UserDbHandler.UpdateUserAsync(reviewee);
                }

                return this.CreatedAtAction(nameof(GetReviewById), new { reviewId = review.ReviewId }, review);
            }
            catch (ArgumentException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        // DELETE: Reviews/00000000-0000-0000-0000-000000000000
        /// <summary>
        /// Deletes a review.
        /// </summary>
        /// <param name="reviewId">The id of the review to delete.</param>
        /// <returns>
        /// Unauthorized() if the logged in user is not the reviewer. <br></br>
        /// Ok() when the review is deleted. <br></br> 
        /// NotFound() if the review does not exist. <br></br>
        /// </returns>
        [HttpDelete("{reviewId}")]
        [Authorize]
        public async Task<ActionResult> DeleteReview(Guid reviewId)
        {
            try
            {
                Review review = await this.ReviewDbHandler.GetReviewAsync(reviewId);

                if (((User)this.HttpContext.Items["User"]).UserId != review.ReviewerId)
                    return this.Unauthorized();

                await this.ReviewDbHandler.DeleteReviewAsync(reviewId);
                return this.Ok();
            }
            catch (ArgumentException e)
            {
                return this.NotFound(e.Message);
            }
        }
    }
}
